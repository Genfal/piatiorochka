package ru.pivasic.carasic.cms.app.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.pivasic.carasic.properties.api.PropertiesService;
import ru.pivasic.carasic.properties.api.PropertyReader;
import ru.pivasic.carasic.properties.api.exception.PropertiesMonitorStartException;
import ru.pivasic.carasic.properties.api.exception.PropertiesNotFoundException;
import ru.pivasic.carasic.properties.api.parser.PropertyReaderFactory;
import ru.pivasic.carasic.properties.impl.PropertiesFileListener;
import ru.pivasic.carasic.properties.impl.PropertiesServiceImpl;
import ru.pivasic.carasic.properties.impl.daemon.DaemonThreadFactory;
import ru.pivasic.carasic.properties.impl.parser.ParseFactoryImpl;

import java.io.File;
import java.util.concurrent.ThreadFactory;

import static ru.pivasic.carasic.properties.api.annotation.ConfigurationEnum.FILES_EXTENSIONS;
import static ru.pivasic.carasic.properties.api.annotation.ConfigurationEnum.FILES_SIZE;

/**
 * Класс для конфигурации пропертей
 *
 * @author Viktor Konev
 * @since 10.08.2022
 */
@Slf4j
@Configuration
public class PropertiesConfiguration {

    @Bean
    public ThreadFactory daemonThreadFactory() {
        return new DaemonThreadFactory();
    }

    @Bean
    public PropertiesService propertiesServiceImpl(@Value("${property.path}") String propertiesPath) {
        PropertiesService propertiesService = new PropertiesServiceImpl(propertiesPath);
        propertiesService.addPropertiesChanged();
        return propertiesService;
    }

    @Bean
    public FileAlterationListener fileAlterationListener(PropertiesService propertiesService) {
        return new PropertiesFileListener(propertiesService);
    }

    @Bean
    public FileAlterationObserver fileAlterationObserver(FileAlterationListener listener, @Value("${property.path}") String propertyPath) {
        File file = new File(propertyPath);
        if (!file.exists()) {
            throw new PropertiesNotFoundException("Файл 'Properties' не был найден по данному пути: " + propertyPath);
        }
        var directoryPath = file.getParentFile();
        FileAlterationObserver observer = new FileAlterationObserver(directoryPath);
        observer.addListener(listener);

        return observer;
    }

    @Bean
    public FileAlterationMonitor fileAlterationMonitor(FileAlterationObserver observer, ThreadFactory daemonThreadFactory, @Value("${property.delay}") Integer interval) {
        FileAlterationMonitor fileAlterationMonitor = new FileAlterationMonitor(interval, observer);
        try {
            fileAlterationMonitor.setThreadFactory(daemonThreadFactory);
            fileAlterationMonitor.start();
        } catch (Exception e) {
            throw new PropertiesMonitorStartException("Ошибка при старте просмотра проперти");
        }
        return fileAlterationMonitor;
    }

    @Bean
    public PropertyReaderFactory parseFactory(PropertiesService propertiesService) {
        ParseFactoryImpl parseFactory = new ParseFactoryImpl(propertiesService);
        parseFactory.init();
        return parseFactory;
    }

    @Bean("FILES_SIZE")
    public PropertyReader<Integer> integerPropertyReader(PropertyReaderFactory propertyReaderFactory) {
        return propertyReaderFactory.create(FILES_SIZE.getKey(), FILES_SIZE.getClazz());
    }

    @Bean("FILES_EXTENSIONS")
    public PropertyReader<String> stringPropertyReader(PropertyReaderFactory propertyReaderFactory) {
        return propertyReaderFactory.create(FILES_EXTENSIONS.getKey(), FILES_EXTENSIONS.getClazz());
    }
}
