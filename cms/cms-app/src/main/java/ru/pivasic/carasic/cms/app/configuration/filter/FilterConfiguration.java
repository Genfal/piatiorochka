package ru.pivasic.carasic.cms.app.configuration.filter;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.pivasic.carasic.commons.impl.filter.ExtensionFileFilter;
import ru.pivasic.carasic.commons.impl.filter.SizeFileFilter;
import ru.pivasic.carasic.properties.api.PropertyReader;

/**
 * Класс конфигурации фильтров
 *
 * @author TorstendasTost
 * @since 13.08.2022
 */
@Configuration
public class FilterConfiguration {

    @Bean
    public FilterRegistrationBean<SizeFileFilter> compareFileSizeRegistration(@Qualifier("FILES_SIZE") PropertyReader<Integer> fileSizePropertyReader) {
        FilterRegistrationBean<SizeFileFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new SizeFileFilter(fileSizePropertyReader));
        registrationBean.addUrlPatterns("/v1/cms/picture/*");
        registrationBean.setOrder(1);
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<ExtensionFileFilter> compareFileExtensionRegistration(@Qualifier("FILES_EXTENSIONS") PropertyReader<String> extensionPropertyReader) {
        FilterRegistrationBean<ExtensionFileFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new ExtensionFileFilter(extensionPropertyReader));
        registrationBean.addUrlPatterns("/v1/cms/picture/*");
        registrationBean.setOrder(2);
        return registrationBean;
    }
}
