package ru.pivasic.carasic.cms.app.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pivasic.carasic.commons.api.dto.UserDtoAuthentication;

/**
 * Контроллер для аутентификации и регистрации
 *
 * @author TorstendasTost
 * @since 19.07.2022
 */
@RestController
@RequestMapping("/cms/authentication")
public class AuthenticationController {

    @PostMapping("/login")
    public String login(@RequestBody UserDtoAuthentication userDtoAuthentication) {
        throw new RuntimeException("Not implemented");
    }

    @PostMapping("/registration")
    public String registration(@RequestBody UserDtoAuthentication userDtoAuthentication) {
        throw new RuntimeException("Not implemented");
    }
}
