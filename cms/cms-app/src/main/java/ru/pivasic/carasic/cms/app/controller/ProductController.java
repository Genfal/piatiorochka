package ru.pivasic.carasic.cms.app.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.pivasic.carasic.database.api.model.Product;

import java.util.List;

/**
 * Контроллер для работы с моделью продукта
 *
 * @author TorstendasTost
 * @since 19.07.2022
 */
@RestController
@RequestMapping("/v1/cms/products")
public class ProductController {

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable Long id) {
        throw new RuntimeException("Not implemented");
    }

    @GetMapping
    public List<Product> getProductList(@RequestParam Integer page, @RequestParam Integer size) {
        throw new RuntimeException("Not implemented");
    }

    @PostMapping
    public Product addProduct(@Validated @RequestBody Product product) {
        throw new RuntimeException("Not implemented");
    }

    @PutMapping
    public Product updateProduct(@Validated @RequestBody Product product) {
        throw new RuntimeException("Not implemented");
    }
}
