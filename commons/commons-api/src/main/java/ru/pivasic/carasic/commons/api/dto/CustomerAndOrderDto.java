package ru.pivasic.carasic.commons.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.pivasic.carasic.database.api.model.Customer;
import ru.pivasic.carasic.database.api.model.Order;

import javax.validation.Valid;

/**
 * {@link Customer} и {@link Order} Dto
 *
 * @author Viktor Konev
 * @since 31.07.2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerAndOrderDto {

    /**
     * Поле покупателя
     */
    @Valid
    Customer customer;

    /**
     * Поле заказа
     */
    @Valid
    Order order;
}
