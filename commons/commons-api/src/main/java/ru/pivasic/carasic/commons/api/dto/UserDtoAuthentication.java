package ru.pivasic.carasic.commons.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * Dto класс для аутентификации
 *
 * @author Viktor Konev
 * @since 31.07.2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDtoAuthentication {

    /**
     * Поле логина пользавателя
     */
    @NotNull
    @Pattern(regexp = "^[\\w.]{1,65}@(yandex|mail|gmail)\\.(ru|com)$",
            message = "Incorrect pattern of login")
    String login;

    /**
     * Поле пароля пользавателя
     */
    @NotNull
    @Pattern(regexp = "^[A-Za-z\\d]{6,25}[.,!]{0,3}$",
            message = "Incorrect pattern of password")
    String password;
}
