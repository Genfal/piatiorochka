package ru.pivasic.carasic.commons.api.exception.filter;

/**
 * Расширение переданного файла не было найдено
 *
 * @author TorstendasTost
 * @since 13.08.2022
 */
public class ExtensionNotFoundException extends RuntimeException {
    public ExtensionNotFoundException(String message) {
        super(message);
    }
}
