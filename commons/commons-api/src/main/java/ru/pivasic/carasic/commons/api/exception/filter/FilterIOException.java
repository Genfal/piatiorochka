package ru.pivasic.carasic.commons.api.exception.filter;

/**
 * Ошибка получаемого файла
 *
 * @author TorstendasTost
 * @since 13.08.2022
 */
public class FilterIOException extends RuntimeException {
    public FilterIOException(String message) {
        super(message);
    }
}
