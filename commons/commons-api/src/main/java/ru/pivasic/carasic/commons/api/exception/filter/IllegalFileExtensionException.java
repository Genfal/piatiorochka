package ru.pivasic.carasic.commons.api.exception.filter;

/**
 * Неправильное расширение переданного файла
 *
 * @author TorstendasTost
 * @since 16.08.2022
 */
public class IllegalFileExtensionException extends RuntimeException {
    public IllegalFileExtensionException(String message) {
        super(message);
    }
}
