package ru.pivasic.carasic.commons.api.exception.filter;

/**
 * Несоответствующий размер переданного файла
 *
 * @author TorstendasTost
 * @since 15.08.2022
 */
public class IllegalFileSizeException extends RuntimeException {
    public IllegalFileSizeException(String message) {
        super(message);
    }
}
