package ru.pivasic.carasic.commons.api.exception.image;

/**
 * (っ˘̩╭╮˘̩)っ сломался
 *
 * @author TorstendasTost
 * @since 08.08.2022
 */
public class ScaleImageException extends RuntimeException {
}
