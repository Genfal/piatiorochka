package ru.pivasic.carasic.commons.api.image;

import ru.pivasic.carasic.commons.api.exception.image.ScaleImageException;

import java.io.InputStream;

/**
 * Интерфейс для нарезки картинок
 *
 * @author TorstendasTost
 * @since 08.08.2022
 */
public interface ImageScale {

    /**
     * Нарезчик картинок
     *
     * @param inputStream Переданная картика
     * @param targetWidth Будущая ширина
     * @param targetHeight Будущая высота
     * @return {@link InputStream} картинка с переданными параметрами
     * @throws ScaleImageException (っ˘̩╭╮˘̩)っ сломался
     */
    InputStream simpleResizeImage(InputStream inputStream, int targetWidth, int targetHeight);
}
