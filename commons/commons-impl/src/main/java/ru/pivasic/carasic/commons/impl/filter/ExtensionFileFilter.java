package ru.pivasic.carasic.commons.impl.filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;
import ru.pivasic.carasic.commons.api.exception.filter.ExtensionNotFoundException;
import ru.pivasic.carasic.commons.api.exception.filter.FilterIOException;
import ru.pivasic.carasic.commons.api.exception.filter.IllegalFileExtensionException;
import ru.pivasic.carasic.properties.api.PropertyReader;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Класс реализации фильтра расширений
 *
 * @author TorstendasTost
 * @since 11.08.2022
 */
@Slf4j
public class ExtensionFileFilter implements Filter {

    private final PropertyReader<String> array;

    public ExtensionFileFilter(PropertyReader<String> array) {
        this.array = array;
    }

    private void compareFileExtension(InputStream inputStream) {
        try {
            List<String> list = array.getList();
            String mimeType = new Tika().detect(inputStream);
            String extension = MimeTypes.getDefaultMimeTypes().forName(mimeType).getAcronym();
            if (!list.contains(extension)) {
                throw new IllegalFileExtensionException("Неправильное расширение файла");
            }
        } catch (IOException | MimeTypeException ioException) {
            throw new FilterIOException("Ошибка получаемого файла");
        } catch (ExtensionNotFoundException extensionNotFoundException) {
            throw new ExtensionNotFoundException("Не удалось найти расширение файла");
        }
        log.info("Расширение изображения найдено");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        compareFileExtension(request.getInputStream());
        chain.doFilter(request, response);
    }
}
