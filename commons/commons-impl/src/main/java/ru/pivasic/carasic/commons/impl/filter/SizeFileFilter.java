package ru.pivasic.carasic.commons.impl.filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import ru.pivasic.carasic.commons.api.exception.filter.FilterIOException;
import ru.pivasic.carasic.commons.api.exception.filter.IllegalFileSizeException;
import ru.pivasic.carasic.commons.impl.filter.dofilter.CachedServletRequestWrapper;
import ru.pivasic.carasic.properties.api.PropertyReader;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;

/**
 * Класс реализации фильтра размера
 *
 * @author TorstendasTost
 * @since 15.08.2022
 */
@Slf4j
public class SizeFileFilter implements Filter {

    PropertyReader<Integer> size;

    public SizeFileFilter(PropertyReader<Integer> size) {
        this.size = size;
    }

    private void compareFileSize(InputStream inputStream) {
        try {
            int bytesSize = size.get() << 20;
            byte[] bytes = IOUtils.toByteArray(inputStream);
            int bytesPictureSize = bytes.length;
            if (bytesPictureSize > bytesSize) {
                throw new IllegalFileSizeException(
                        "файл весит: "
                        + FileUtils.byteCountToDisplaySize(bytesPictureSize)
                        + ", а должен весить: "
                        + FileUtils.byteCountToDisplaySize(bytesSize));
            }
        } catch (IOException ioException) {
            throw new FilterIOException("Ошибка получаемого файла");
        }
        log.info("Размер изображения соответствует");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        CachedServletRequestWrapper wrapper = new CachedServletRequestWrapper((HttpServletRequest) request);
        compareFileSize(wrapper.getInputStream());
        chain.doFilter(wrapper, response);
    }
}
