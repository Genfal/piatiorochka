package ru.pivasic.carasic.commons.impl.filter.dofilter;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Класс для работы doFilter
 *
 * @author TorstendasTost
 * @since 15.08.2022
 */
public class CachedInputStream extends ServletInputStream {

    private final InputStream cashedInputStream;

    public CachedInputStream(byte[] cashedBody) {
        this.cashedInputStream = new ByteArrayInputStream(cashedBody);
    }

    @Override
    public boolean isFinished() {
        try {
            return cashedInputStream.available() == 0;
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public int read() {
        try {
            return cashedInputStream.read();
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    @Override
    public void setReadListener(ReadListener readListener) {
    }
}
