package ru.pivasic.carasic.commons.impl.filter.dofilter;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;

/**
 * Класс для работы doFilter
 *
 * @author TorstendasTost
 * @since 15.08.2022
 */
public class CachedServletRequestWrapper extends HttpServletRequestWrapper {

    private final byte[] cachedBody;

    public CachedServletRequestWrapper(HttpServletRequest request) {
        super(request);
        try {
            this.cachedBody = IOUtils.toByteArray(request.getInputStream());
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    @Override
    public ServletInputStream getInputStream() {
        return new CachedInputStream(this.cachedBody);
    }
}

