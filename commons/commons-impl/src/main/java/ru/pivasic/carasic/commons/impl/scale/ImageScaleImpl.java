package ru.pivasic.carasic.commons.impl.scale;

import lombok.extern.slf4j.Slf4j;
import org.imgscalr.Scalr;
import ru.pivasic.carasic.commons.api.exception.image.ScaleImageException;
import ru.pivasic.carasic.commons.api.image.ImageScale;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Класс реализации {@link ImageScale}
 *
 * @author TorstendasTost
 * @since 07.08.2022
 */
@Slf4j
public class ImageScaleImpl implements ImageScale {

    public InputStream simpleResizeImage(InputStream inputStream, int targetWidth, int targetHeight) {
        try {
            BufferedImage image = ImageIO.read(inputStream);
            BufferedImage scaledImage = Scalr.resize(image, Scalr.Method.BALANCED, Scalr.Mode.FIT_EXACT, targetWidth, targetHeight);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(scaledImage, "jpg", byteArrayOutputStream);
            return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        } catch (IOException ioException) {
            log.debug("(っ˘̩╭╮˘̩)っ сломался");
            throw new ScaleImageException();
        }
    }
}
