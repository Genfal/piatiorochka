package ru.pivasic.carasic.commons.impl.scale;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import ru.pivasic.carasic.commons.api.exception.image.ScaleImageException;
import ru.pivasic.carasic.commons.api.image.ImageScale;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author TorstendasTost
 * @since 08.08.2022
 */
class ImageScaleImplTest {

    private static final Integer WIDTH = 100;

    private static final Integer HEIGHT = 100;

    private ImageScale imageScale;

    private InputStream inputStream;

    @BeforeEach
    void init() throws IOException {
        imageScale = new ImageScaleImpl();
        inputStream = new FileInputStream("src/test/resources/EngfvFsFYkA.jpg");
    }

    @AfterEach
    void end() throws IOException {
        inputStream.close();
    }

    @Test
    void simpleResizeImage() throws IOException {
        InputStream finalInput = imageScale.simpleResizeImage(inputStream, WIDTH, HEIGHT);
        BufferedImage bufferedImage = ImageIO.read(finalInput);

        Truth.assertThat(bufferedImage.getWidth()).isEqualTo(WIDTH);
        Truth.assertThat(bufferedImage.getHeight()).isEqualTo(HEIGHT);
    }

    @Test
    void scaleImageException() {
        try (MockedStatic<ImageIO> mockedStatic = Mockito.mockStatic(ImageIO.class)) {
            mockedStatic.when(() -> ImageIO.read(inputStream)).thenThrow(IOException.class);

            Assertions.assertThrows(ScaleImageException.class, () -> imageScale.simpleResizeImage(inputStream, WIDTH, HEIGHT));
        }
    }
}