package ru.pivasic.carasic.courier.app.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.pivasic.carasic.properties.api.PropertiesService;
import ru.pivasic.carasic.properties.api.exception.PropertiesMonitorStartException;
import ru.pivasic.carasic.properties.api.exception.PropertiesNotFoundException;
import ru.pivasic.carasic.properties.impl.PropertiesFileListener;
import ru.pivasic.carasic.properties.impl.PropertiesServiceImpl;
import ru.pivasic.carasic.properties.impl.daemon.DaemonThreadFactory;

import java.io.File;
import java.util.concurrent.ThreadFactory;

/**
 * Класс для конфигурации пропертей
 *
 * @author Viktor Konev
 * @since 13.08.2022
 */
@Slf4j
@Configuration
public class PropertiesConfiguration {

    @Bean
    public ThreadFactory daemonThreadFactory() {
        return new DaemonThreadFactory();
    }

    @Bean
    public PropertiesService propertiesServiceImpl(@Value("${property.path}") String propertyPath) {
        PropertiesService propertiesService = new PropertiesServiceImpl(propertyPath);
        propertiesService.addPropertiesChanged();
        return propertiesService;
    }

    @Bean
    public FileAlterationListener propertiesFileListener(PropertiesService propertiesService) {
        return new PropertiesFileListener(propertiesService);
    }

    @Bean
    public FileAlterationObserver fileAlterationObserver(FileAlterationListener listener, @Value("${property.path}") String propertyPath) {
        File file = new File(propertyPath);
        if (!file.exists()) {
            throw new PropertiesNotFoundException("Файл 'Properties' не был найден по данному пути: " + propertyPath);
        }
        var directoryPath = file.getParentFile();
        FileAlterationObserver fileAlterationObserver = new FileAlterationObserver(directoryPath);
        fileAlterationObserver.addListener(listener);

        return fileAlterationObserver;
    }

    @Bean
    public FileAlterationMonitor fileAlterationMonitor(FileAlterationObserver observer, ThreadFactory daemonThreadFactory, @Value("${property.delay}") Integer interval) {
        FileAlterationMonitor fileAlterationMonitor = new FileAlterationMonitor(interval, observer);
        try {
            fileAlterationMonitor.setThreadFactory(daemonThreadFactory);
            fileAlterationMonitor.start();
        } catch (Exception e) {
            throw new PropertiesMonitorStartException("Ошибка при старте просмотра проперти");
        }
        return fileAlterationMonitor;
    }
}
