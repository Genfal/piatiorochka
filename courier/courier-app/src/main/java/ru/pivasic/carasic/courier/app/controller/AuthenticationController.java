package ru.pivasic.carasic.courier.app.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pivasic.carasic.commons.api.dto.UserDtoAuthentication;

/**
 * Контроллер для аутентификации и регистрации курьера
 *
 * @author TorstendasTost
 * @since 20.07.2022
 */
@RestController
@RequestMapping("/courier/authentication")
public class AuthenticationController {

    @PostMapping("/login")
    public String login(@RequestBody UserDtoAuthentication userDtoAuthentication) {
        throw new RuntimeException("Not implemented");
    }

    @PostMapping("/registration")
    public String registration(@RequestBody UserDtoAuthentication userDtoAuthentication) {
        throw new RuntimeException("Not implemented");
    }
}
