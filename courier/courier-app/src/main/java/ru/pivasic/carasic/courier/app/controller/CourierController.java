package ru.pivasic.carasic.courier.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.pivasic.carasic.database.api.model.Order;
import ru.pivasic.carasic.database.api.model.Status;

import java.util.List;

/**
 * Контроллер для работы курьера с моделью заказа
 *
 * @author TorstendasTost
 * @since 20.07.2022
 */
@RestController
@RequestMapping("/v1/courier/deliveries")
public class CourierController {

    @GetMapping("/{id}")
    public Order getDelivery(@PathVariable Long id) {
        throw new RuntimeException("Not implemented");
    }

    @GetMapping
    public List<Order> getDeliveryList(@RequestParam Integer size, @RequestParam Integer page) {
        throw new RuntimeException("Not implemented");
    }

    @PatchMapping("/status")
    public Order patchStatusId(@RequestBody Status status) {
        throw new RuntimeException("Not implemented");
    }
}
