package ru.pivasic.carasic.database.api.exception;

/**
 * База данных недоступна
 *
 * @author TorstendasTost
 * @since 01.08.2022
 */
public class DatabaseUnavailable extends RuntimeException{
}
