package ru.pivasic.carasic.database.api.exception.order;

import ru.pivasic.carasic.database.api.model.Order;

/**
 * {@link Order} с пустым полем id
 *
 * @author TorstendasTost
 * @since 31.07.2022
 */
public class NullOrderIdException extends RuntimeException{
}
