package ru.pivasic.carasic.database.api.exception.order;

import ru.pivasic.carasic.database.api.model.Order;

/**
 * {@link Order} не найден в базе данных
 *
 * @author TorstendasTost
 * @since 31.07.2022
 */
public class OrderNotFoundException extends RuntimeException{
}
