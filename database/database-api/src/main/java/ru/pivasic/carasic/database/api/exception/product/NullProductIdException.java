package ru.pivasic.carasic.database.api.exception.product;

import ru.pivasic.carasic.database.api.model.Product;

/**
 * {@link Product} с пустым полем id
 *
 * @author TorstendasTost
 * @since 06.08.2022
 */
public class NullProductIdException extends RuntimeException {
}
