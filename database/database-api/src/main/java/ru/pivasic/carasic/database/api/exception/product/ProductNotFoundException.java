package ru.pivasic.carasic.database.api.exception.product;

import ru.pivasic.carasic.database.api.model.Product;

/**
 * {@link Product} не найден в базе данных
 *
 * @author TorstendasTost
 * @since 06.08.2022
 */
public class ProductNotFoundException extends RuntimeException {
}
