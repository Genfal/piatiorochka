package ru.pivasic.carasic.database.api.exception.stack;

import ru.pivasic.carasic.database.api.model.Stack;

/**
 * {@link Stack} с пустым полем id
 *
 * @author TorstendasTost
 * @since 04.08.2022
 */
public class NullStackIdException extends RuntimeException{
}
