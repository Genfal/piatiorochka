package ru.pivasic.carasic.database.api.exception.stack;

import ru.pivasic.carasic.database.api.model.Stack;

/**
 * {@link Stack} не найден в базе данных
 *
 * @author TorstendasTost
 * @since 04.08.2022
 */
public class StackNotFoundException extends RuntimeException{
}
