package ru.pivasic.carasic.database.api.model;

/**
 * Категория продукта
 *
 * @author TorstendasTost
 * @since 23.07.2022
 */
public enum Category {
    DAIRY,
    BAKERY,
    VEGETABLES,
    MEAT,
    GROCERY,
    WATER
}
