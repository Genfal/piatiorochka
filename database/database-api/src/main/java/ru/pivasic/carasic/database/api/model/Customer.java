package ru.pivasic.carasic.database.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * Модель покупателя
 *
 * @author Viktor Konev
 * @since 22.07.2022
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "address", length = 80, nullable = false)
    @Pattern(regexp = "^[А-Яа-я\\d. ]{15,80}$",
            message = "Incorrect pattern of address")
    private String address;

    @NotNull
    @Column(name = "phone_number", length = 15, nullable = false)
    @Pattern(regexp = "^(8|\\+7)((| |-)(\\d{3})"
                    + "(-| |)|)(\\d{3})"
                    + "(-| |)(\\d{2})"
                    + "(-| |)(\\d{2})$",
            message = "Incorrect pattern of message")
    private String phoneNumber;

    @NotNull
    @Column(name = "name", length = 30, nullable = false)
    @Pattern(regexp = "^[А-Яа-я]{2,30}$",
            message = "Incorrect pattern of name")
    private String name;

    @NotNull
    @Column(name = "surname", length = 30)
    @Pattern(regexp = "^[А-Яа-я]{2,30}$",
            message = "Incorrect pattern of surname")
    private String surname;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
    private List<Order> orderList;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Customer customer = (Customer) o;

        return new EqualsBuilder()
                .append(id, customer.id)
                .append(address, customer.address)
                .append(phoneNumber, customer.phoneNumber)
                .append(name, customer.name)
                .append(surname, customer.surname)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(address)
                .append(phoneNumber)
                .append(name)
                .append(surname)
                .toHashCode();
    }
}