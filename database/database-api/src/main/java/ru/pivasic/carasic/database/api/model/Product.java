package ru.pivasic.carasic.database.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

/**
 * Модель продукта
 *
 * @author TorstendasTost
 * @since 23.07.2022
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    @Pattern(message = "Неподходящее имя продукта: ${validatedValue}",
             regexp = "^[\\w\\sА-я\\p{Punct}]{1,100}$")
    private String name;

    @NotNull
    @Column(name = "description", nullable = false)
    @Pattern(message = "Неподходящее описание продукта: ${validatedValue}",
             regexp = "^[\\w\\sА-я\\p{Punct}]{1,400}$")
    private String description;

    @NotNull
    @Column(name = "picture_path", nullable = false)
    @Pattern(message = "Неподходящий путь к изображению: ${validatedValue}",
             regexp = "^\\/pyatorochka"
                     + "\\/product"
                     + "\\/[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}"
                     + "\\/(small|medium|high)"
                     + "\\/[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}\\.(png|jpg)$")
    private String picturePath;

    @NotNull
    @Positive
    @Column(name = "price", nullable = false)
    private Integer price;

    @NotNull
    @Column(name = "category", nullable = false)
    @Enumerated(EnumType.STRING)
    private Category category;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return new EqualsBuilder()
                .append(id, product.id)
                .append(name, product.name)
                .append(description, product.description)
                .append(picturePath, product.picturePath)
                .append(price, product.price)
                .append(category, product.category)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(description)
                .append(picturePath)
                .append(price)
                .append(category)
                .toHashCode();
    }
}
