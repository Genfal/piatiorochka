package ru.pivasic.carasic.database.api.model;

/**
 * Роль пользователя
 *
 * @author Viktor Konev
 * @since 21.07.2022
 */
public enum Role {
    USER,
    COURIER,
    ADMIN,
    CONTENT_MANAGER
}