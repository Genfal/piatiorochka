package ru.pivasic.carasic.database.api.model;

/**
 * Статус заказа
 *
 * @author TorstendasTost
 * @since 23.07.2022
 */
public enum Status {
    CREATED,
    IN_DELIVERY,
    DELIVERED
}
