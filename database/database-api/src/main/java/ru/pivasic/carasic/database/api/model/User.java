package ru.pivasic.carasic.database.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * Модель пользователя
 *
 * @author Viktor Konev
 * @since 21.07.2022
 */
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "user_table")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "login", length = 80, nullable = false, unique = true)
    @Pattern(regexp = "^[\\w.]{1,65}@(yandex|mail|gmail)\\.(ru|com)$",
            message = "Incorrect pattern of login")
    private String login;

    @NotNull
    @Column(name = "password", length = 25, nullable = false)
    @Pattern(regexp = "^[A-Za-z\\d]{6,25}[.,!]{0,3}$",
            message = "Incorrect pattern of password")
    private String password;

    @Column(name = "verification_code")
    @Pattern(regexp = "^\\d{5}$",
            message = "Incorrect pattern of verification code")
    private String verificationCode;

    @NotNull
    @Column(name = "jwt",nullable = false, unique = true)
    @Pattern(regexp = "^[A-Za-z\\d-_]*\\.[A-Za-z\\d-_]"
                    + "*\\.[A-Za-z\\d-_]*$",
            message = "Incorrect pattern of JWT")
    private String jwt;

    @NotNull
    @Column(name = "avatar_path")
    @Pattern(regexp = "^/pyatorochka/user/[\\da-f]{8}-"
                    + "[\\da-f]{4}-[\\da-f]{4}-"
                    + "[\\da-f]{4}-[\\da-f]{12}/(small|medium|high)"
                    + "/[\\da-f]{8}-[\\da-f]{4}-"
                    + "[\\da-f]{4}-[\\da-f]{4}-"
                    + "[\\da-f]{12}\\.(jpg|png)$",
            message = "Incorrect pattern of avatar path")
    private String avatarPath;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Customer> customer;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;

        return new EqualsBuilder()
                .append(id, user.id)
                .append(login, user.login)
                .append(password, user.password)
                .append(verificationCode, user.verificationCode)
                .append(avatarPath, user.avatarPath)
                .append(role, user.role)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(login)
                .append(password)
                .append(verificationCode)
                .append(avatarPath)
                .append(role)
                .toHashCode();
    }
}