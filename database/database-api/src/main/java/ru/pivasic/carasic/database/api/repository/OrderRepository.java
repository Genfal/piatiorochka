package ru.pivasic.carasic.database.api.repository;

import lombok.NonNull;
import ru.pivasic.carasic.database.api.exception.DatabaseUnavailable;
import ru.pivasic.carasic.database.api.model.Order;

import java.util.Optional;

/**
 * Интерфейс для работы с моделью {@link Order} в базе данных
 *
 * @author TorstendasTost
 * @since 31.07.2022
 */
public interface OrderRepository {

    /**
     * Сохранение {@link Order}
     *
     * @param order {@link Order} который будет сохранён
     * @return Полученный {@link Order} с id
     * @throws DatabaseUnavailable база данных недоступна
     */
    Order save(@NonNull Order order);

    /**
     * Получение {@link Order}
     *
     * @param id Идентификатор по которому будет найден {@link Order}
     * @return {@link Optional<Order>} который был найден по id
     * @throws DatabaseUnavailable база данных недоступна
     */
    Optional<Order> find(@NonNull Long id);

    /**
     * Обновление {@link Order}
     *
     * @param order {@link Order} который будет обновлен
     * @return Обновлённый {@link Order}
     * @throws DatabaseUnavailable база данных недоступна
     */
    Order update(@NonNull Order order);

    /**
     * Удаление {@link Order}
     *
     * @param id Идентификатор по которому {@link Order} будет удалён
     * @throws DatabaseUnavailable база данных недоступна
     */
    void delete(@NonNull Long id);
}
