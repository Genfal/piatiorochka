package ru.pivasic.carasic.database.api.repository;

import lombok.NonNull;
import ru.pivasic.carasic.database.api.exception.DatabaseUnavailable;
import ru.pivasic.carasic.database.api.model.Product;

import java.util.List;
import java.util.Optional;

/**
 * Интерфейс для работы с моделью {@link Product} в базе данных
 *
 * @author TorstendasTost
 * @since 05.08.2022
 */
public interface ProductRepository {

    /**
     * Сохранение {@link Product}
     *
     * @param product {@link Product} который будет сохранён
     * @return Полученный {@link Product} с id
     * @throws DatabaseUnavailable база данных недоступна
     */
    Product save(@NonNull Product product);

    /**
     * Получение {@link Product}
     *
     * @param id Идентификатор по которому будет найден {@link Product}
     * @return {@link Optional<Product>} который был найден по id
     * @throws DatabaseUnavailable база данных недоступна
     */
    Optional<Product> find(@NonNull Long id);

    /**
     * Получение всех {@link Product} с нумерацией страниц
     *
     * @param page Номер страницы
     * @param size Количество {@link Product} на странице
     * @return {@link List<Product>} на найденной странице
     * @throws DatabaseUnavailable база данных недоступна
     */
    List<Product> findAll(@NonNull Integer page, @NonNull Integer size);

    /**
     * Обновление {@link Product}
     *
     * @param product {@link Product} который будет обновлен
     * @return Обновлённый {@link Product}
     * @throws DatabaseUnavailable база данных недоступна
     */
    Product update(@NonNull Product product);

    /**
     * Удаление {@link Product}
     *
     * @param id Идентификатор по которому {@link Product} будет удалён
     * @throws DatabaseUnavailable база данных недоступна
     */
    void delete(@NonNull Long id);
}
