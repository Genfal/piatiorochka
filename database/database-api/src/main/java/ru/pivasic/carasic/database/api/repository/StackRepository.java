package ru.pivasic.carasic.database.api.repository;

import lombok.NonNull;
import ru.pivasic.carasic.database.api.exception.DatabaseUnavailable;
import ru.pivasic.carasic.database.api.model.Stack;

import java.util.Optional;

/**
 * Интерфейс для работы с моделью {@link Stack} в базе данных
 *
 * @author TorstendasTost
 * @since 04.08.2022
 */
public interface StackRepository {

    /**
     * Сохранение {@link Stack}
     *
     * @param stack {@link Stack} который будет сохранён
     * @return Полученный {@link Stack} с id
     * @throws DatabaseUnavailable база данных недоступна
     */
    Stack save(@NonNull Stack stack);

    /**
     * Получение {@link Stack}
     *
     * @param id Идентификатор по которому будет найден {@link Stack}
     * @return {@link Optional<Stack>} который был найден по id
     * @throws DatabaseUnavailable база данных недоступна
     */
    Optional<Stack> find(@NonNull Long id);

    /**
     * Обновление {@link Stack}
     *
     * @param stack {@link Stack} который будет обновлён
     * @return Обновлённый {@link Stack}
     * @throws DatabaseUnavailable база данных недоступна
     */
    Stack update(@NonNull Stack stack);

    /**
     * Удаление {@link Stack}
     *
     * @param id Идентификатор по которому {@link Stack} будет удалён
     * @throws DatabaseUnavailable база данных недоступна
     */
    void delete(@NonNull Long id);
}
