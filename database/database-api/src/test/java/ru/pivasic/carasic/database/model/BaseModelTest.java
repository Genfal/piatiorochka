package ru.pivasic.carasic.database.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;

/**
 * @author TorstendasTost
 * @since 27.07.2022
 */
public abstract class BaseModelTest<T> {

    private T baseObject;
    private T equalsToBaseObject;
    private List<T> noneEqualsObjects;

    protected abstract T baseObject();
    protected abstract T equalsToBaseObject();
    protected abstract List<T> noneEqualsObjects();

    @BeforeEach
    protected void setUp(){
        baseObject = baseObject();
        equalsToBaseObject = equalsToBaseObject();
        noneEqualsObjects = noneEqualsObjects();
    }


    @Test
    @DisplayName("Тест equals и hashCode")
    protected void testEqualsHashCodeTrue(){
        assertThat(baseObject).isEqualTo(equalsToBaseObject);
        assertThat(baseObject.hashCode()).isEqualTo(equalsToBaseObject.hashCode());
    }

    @Test
    @DisplayName("Тест листа на отсутсвие совпадений с базовым продуктом")
    protected void testEqualsHashCodeFalse() {
        noneEqualsObjects.forEach(el -> assertThat(baseObject).isNotEqualTo(el));
        noneEqualsObjects.forEach(el -> assertThat(baseObject.hashCode()).isNotEqualTo(el.hashCode()));
    }

    @Test
    @DisplayName("Тест на Equals одинаковых объектов")
    protected void testOfEqualsObjectTrue(){
        assertThat(baseObject.equals(baseObject)).isTrue();
    }

    @Test
    @DisplayName("Тест на null")
    protected void testOfNullFalse(){
        assertThat(baseObject.equals(null)).isFalse();
    }

    @Test
    @DisplayName("Тест на разные классы")
    protected void testForDifferentClasses(){
        assertThat(baseObject.getClass().equals(Object.class)).isFalse();
    }
}
