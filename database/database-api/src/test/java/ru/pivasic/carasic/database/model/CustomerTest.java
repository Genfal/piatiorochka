package ru.pivasic.carasic.database.model;

import ru.pivasic.carasic.database.api.model.Customer;
import ru.pivasic.carasic.database.api.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Viktor Konev
 * @since 30.07.2022
 */
class CustomerTest extends BaseModelTest<Customer> {
    @Override
    protected Customer baseObject() {
        return Customer.builder()
                .id(1L)
                .address("ул.Красная 42")
                .phoneNumber("89888662454")
                .name("Виктор")
                .surname("Конев")
                .build();
    }

    @Override
    protected Customer equalsToBaseObject() {
        return Customer.builder()
                .id(1L)
                .address("ул.Красная 42")
                .phoneNumber("89888662454")
                .name("Виктор")
                .surname("Конев")
                .orderList(new ArrayList<>())
                .user(User.builder().build())
                .build();
    }

    @Override
    protected List<Customer> noneEqualsObjects() {
        return List.of(
                Customer.builder().id(2L).address("ул.Красная 42").phoneNumber("89888662454").name("Виктор").surname("Конев").build(),
                Customer.builder().id(1L).address("ул.Шоссейная 20").phoneNumber("89888662454").name("Виктор").surname("Конев").build(),
                Customer.builder().id(1L).address("ул.Красная 42").phoneNumber("898886").name("Виктор").surname("Конев").build(),
                Customer.builder().id(1L).address("ул.Красная 42").phoneNumber("89888662454").name("Александр").surname("Конев").build(),
                Customer.builder().id(1L).address("ул.Красная 42").phoneNumber("89888662454").name("Виктор").surname("Данилов").build()
        );
    }
}
