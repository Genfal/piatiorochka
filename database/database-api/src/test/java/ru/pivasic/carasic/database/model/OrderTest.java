package ru.pivasic.carasic.database.model;

import ru.pivasic.carasic.database.api.model.Customer;
import ru.pivasic.carasic.database.api.model.Order;
import ru.pivasic.carasic.database.api.model.Status;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TorstendasTost
 * @since 29.07.2022
 */
class OrderTest extends BaseModelTest<Order> {

    @Override
    protected Order baseObject() {
        return Order.builder()
                .id(1L)
                .price(200)
                .status(Status.CREATED)
                .build();
    }

    @Override
    protected Order equalsToBaseObject() {
        return Order.builder()
                .id(1L)
                .price(200)
                .status(Status.CREATED)
                .customer(Customer.builder().build())
                .stackList(new ArrayList<>())
                .build();
    }

    @Override
    protected List<Order> noneEqualsObjects() {
        return List.of(
                Order.builder().id(2L).price(200).status(Status.CREATED).build(),
                Order.builder().id(1L).price(400).status(Status.CREATED).build(),
                Order.builder().id(1L).price(200).status(Status.DELIVERED).build());
    }
}