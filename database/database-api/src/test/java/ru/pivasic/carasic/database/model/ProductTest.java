package ru.pivasic.carasic.database.model;

import ru.pivasic.carasic.database.api.model.Category;
import ru.pivasic.carasic.database.api.model.Product;

import java.util.List;

/**
 * @author TorstendasTost
 * @since 27.07.2022
 */
class ProductTest extends BaseModelTest<Product> {

    @Override
    protected Product baseObject() {
        return Product.builder()
                .id(1L)
                .name("Milk")
                .description("milk is milk")
                .picturePath("picturePath")
                .price(200)
                .category(Category.DAIRY)
                .build();
    }

    @Override
    protected Product equalsToBaseObject() {
        return Product.builder()
                .id(1L)
                .name("Milk")
                .description("milk is milk")
                .picturePath("picturePath")
                .price(200)
                .category(Category.DAIRY)
                .build();
    }

    @Override
    protected List<Product> noneEqualsObjects() {
        return List.of(
                Product.builder().id(2L).name("Milk").description("milk is milk").picturePath("picturePath").price(200).category(Category.DAIRY).build(),
                Product.builder().id(1L).name("another").description("milk is milk").picturePath("picturePath").price(200).category(Category.DAIRY).build(),
                Product.builder().id(1L).name("Milk").description("another").picturePath("picturePath").price(200).category(Category.DAIRY).build(),
                Product.builder().id(1L).name("Milk").description("milk is milk").picturePath("another").price(200).category(Category.DAIRY).build(),
                Product.builder().id(1L).name("Milk").description("milk is milk").picturePath("picturePath").price(400).category(Category.DAIRY).build(),
                Product.builder().id(1L).name("Milk").description("milk is milk").picturePath("picturePath").price(200).category(Category.MEAT).build());
    }
}
