package ru.pivasic.carasic.database.model;

import ru.pivasic.carasic.database.api.model.Order;
import ru.pivasic.carasic.database.api.model.Product;
import ru.pivasic.carasic.database.api.model.Stack;

import java.util.List;

/**
 * @author TorstendasTost
 * @since 29.07.2022
 */
class StackTest extends BaseModelTest<Stack> {

    @Override
    protected Stack baseObject() {
        return Stack.builder()
                .id(1L)
                .quantity(3)
                .build();
    }

    @Override
    protected Stack equalsToBaseObject() {
        return Stack.builder()
                .id(1L)
                .quantity(3)
                .order(Order.builder().build())
                .product(Product.builder().build())
                .build();
    }

    @Override
    protected List<Stack> noneEqualsObjects() {
        return List.of(
                Stack.builder().id(2L).quantity(3).build(),
                Stack.builder().id(1L).quantity(4).build());
    }
}