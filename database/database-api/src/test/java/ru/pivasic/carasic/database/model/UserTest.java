package ru.pivasic.carasic.database.model;

import ru.pivasic.carasic.database.api.model.Role;
import ru.pivasic.carasic.database.api.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Viktor Konev
 * @since 30.07.2022
 */
class UserTest extends BaseModelTest<User> {

    @Override
    protected User baseObject() {
        return User.builder()
                .id(1L)
                .login("vins.conev")
                .password("14448")
                .verificationCode("45444")
                .avatarPath("avatarPath")
                .role(Role.USER)
                .build();
    }

    @Override
    protected User equalsToBaseObject() {
        return User.builder()
                .id(1L)
                .login("vins.conev")
                .password("14448")
                .verificationCode("45444")
                .jwt("Jwt")
                .avatarPath("avatarPath")
                .role(Role.USER)
                .customer(new ArrayList<>())
                .build();
    }

    @Override
    protected List<User> noneEqualsObjects() {
        return List.of(
                User.builder().id(2L).login("vins.conev").password("14448").verificationCode("4544").avatarPath("avatarPath").role(Role.USER).build(),
                User.builder().id(1L).login("egor.torsten").password("14448").verificationCode("4544").avatarPath("avatarPath").role(Role.USER).build(),
                User.builder().id(1L).login("vins.conev").password("sfffes").verificationCode("4544").avatarPath("avatarPath").role(Role.USER).build(),
                User.builder().id(1L).login("vins.conev").password("14448").verificationCode("3344").avatarPath("avatarPath").role(Role.USER).build(),
                User.builder().id(1L).login("vins.conev").password("14448").verificationCode("4544").avatarPath("AnotherAvatarPath").role(Role.USER).build(),
                User.builder().id(1L).login("vins.conev").password("14448").verificationCode("4544").avatarPath("avatarPath").role(Role.ADMIN).build()
        );
    }
}
