package ru.pivasic.carasic.database.impl.repository;

import lombok.NonNull;
import ru.pivasic.carasic.database.api.exception.DatabaseUnavailable;
import ru.pivasic.carasic.database.api.model.Order;
import ru.pivasic.carasic.database.api.repository.OrderRepository;
import ru.pivasic.carasic.database.impl.repository.spring.jpa.OrderCrudRepository;

import java.util.Optional;

/**
 * Класс реализации {@link OrderRepository}
 *
 * @author TorstendasTost
 * @since 31.07.2022
 */
public class OrderRepositoryImpl implements OrderRepository {

    private final OrderCrudRepository orderCrudRepository;

    public OrderRepositoryImpl(OrderCrudRepository orderCrudRepository) {
        this.orderCrudRepository = orderCrudRepository;
    }

    @Override
    public Order save(@NonNull Order order) {
        try {
            return orderCrudRepository.save(order);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }

    @Override
    public Optional<Order> find(@NonNull Long id) {
        try {
            return orderCrudRepository.findById(id);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }

    @Override
    public Order update(@NonNull Order order) {
        try {
            return orderCrudRepository.save(order);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }

    @Override
    public void delete(@NonNull Long id) {
        try {
            orderCrudRepository.deleteById(id);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }
}
