package ru.pivasic.carasic.database.impl.repository;

import lombok.NonNull;
import org.springframework.data.domain.PageRequest;
import ru.pivasic.carasic.database.api.exception.DatabaseUnavailable;
import ru.pivasic.carasic.database.api.model.Product;
import ru.pivasic.carasic.database.api.repository.ProductRepository;
import ru.pivasic.carasic.database.impl.repository.spring.jpa.ProductPagingRepository;

import java.util.List;
import java.util.Optional;

/**
 * Класс реализации {@link ProductRepository}
 *
 * @author TorstendasTost
 * @since 05.08.2022
 */
public class ProductRepositoryImpl implements ProductRepository {

    private final ProductPagingRepository productPagingRepository;

    public ProductRepositoryImpl(ProductPagingRepository productPagingRepository) {
        this.productPagingRepository = productPagingRepository;
    }

    @Override
    public Product save(@NonNull Product product) {
        try {
            return productPagingRepository.save(product);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }

    @Override
    public Optional<Product> find(@NonNull Long id) {
        try {
            return productPagingRepository.findById(id);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }

    @Override
    public List<Product> findAll(@NonNull Integer page, @NonNull Integer size) {
        try {
            return productPagingRepository.findAll(PageRequest.of(page, size)).toList();
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }

    @Override
    public Product update(@NonNull Product product) {
        try {
            return productPagingRepository.save(product);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }

    @Override
    public void delete(@NonNull Long id) {
        try {
            productPagingRepository.deleteById(id);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }
}
