package ru.pivasic.carasic.database.impl.repository;

import lombok.NonNull;
import ru.pivasic.carasic.database.api.exception.DatabaseUnavailable;
import ru.pivasic.carasic.database.api.model.Stack;
import ru.pivasic.carasic.database.api.repository.StackRepository;
import ru.pivasic.carasic.database.impl.repository.spring.jpa.StackCrudRepository;

import java.util.Optional;

/**
 * Класс реализации {@link StackRepository}
 *
 * @author TorstendasTost
 * @since 04.08.2022
 */
public class StackRepositoryImpl implements StackRepository {

    private final StackCrudRepository stackCrudRepository;

    public StackRepositoryImpl(StackCrudRepository stackCrudRepository) {
        this.stackCrudRepository = stackCrudRepository;
    }

    @Override
    public Stack save(@NonNull Stack stack) {
        try {
            return stackCrudRepository.save(stack);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }

    @Override
    public Optional<Stack> find(@NonNull Long id) {
        try {
            return stackCrudRepository.findById(id);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }

    @Override
    public Stack update(@NonNull Stack stack) {
        try {
            return stackCrudRepository.save(stack);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }

    @Override
    public void delete(@NonNull Long id) {
        try {
            stackCrudRepository.deleteById(id);
        } catch (RuntimeException runtimeException) {
            throw new DatabaseUnavailable();
        }
    }
}
