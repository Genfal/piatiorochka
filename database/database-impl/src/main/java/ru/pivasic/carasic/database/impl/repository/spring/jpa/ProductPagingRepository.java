package ru.pivasic.carasic.database.impl.repository.spring.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.pivasic.carasic.database.api.model.Product;

/**
 * Репозиторий для работы с {@link Product}
 *
 * @author TorstendasTost
 * @since 30.07.2022
 */
@Repository
public interface ProductPagingRepository extends PagingAndSortingRepository<Product, Long> {
}
