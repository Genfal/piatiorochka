package ru.pivasic.carasic.database.impl.repository.spring.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.pivasic.carasic.database.api.model.Stack;

/**
 * Репозиторий для работы с {@link Stack}
 *
 * @author TorstendasTost
 * @since 31.07.2022
 */
@Repository
public interface StackCrudRepository extends CrudRepository<Stack, Long> {

}
