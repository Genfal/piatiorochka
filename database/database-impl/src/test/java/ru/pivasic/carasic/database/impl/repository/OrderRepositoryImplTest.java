package ru.pivasic.carasic.database.impl.repository;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.pivasic.carasic.database.api.exception.DatabaseUnavailable;
import ru.pivasic.carasic.database.api.model.Order;
import ru.pivasic.carasic.database.api.repository.OrderRepository;
import ru.pivasic.carasic.database.impl.repository.spring.jpa.OrderCrudRepository;

import java.util.Optional;

/**
 * @author TorstendasTost
 * @since 02.08.2022
 */
class OrderRepositoryImplTest {

    private static final Long ID = 1L;

    private static final Order ORDER_WITH_ID = Order.builder().id(ID).build();

    private static final Order ORDER_WO_ID = Order.builder().build();

    private static final Optional<Order> OPTIONAL_ORDER = Optional.of(ORDER_WITH_ID);

    private OrderCrudRepository orderCrudRepository;

    private OrderRepository orderRepository;

    @BeforeEach
    void init() {
        orderCrudRepository = Mockito.mock(OrderCrudRepository.class);
        orderRepository = new OrderRepositoryImpl(orderCrudRepository);
    }

    @Test
    void findSuccess() {
        Mockito.when(orderCrudRepository.findById(ID)).thenReturn(OPTIONAL_ORDER);

        Truth.assertThat(orderRepository.find(ID)).isEqualTo(OPTIONAL_ORDER);

        Mockito.verify(orderCrudRepository).findById(ID);
    }

    @Test
    void findDatabaseUnavailable() {
        Mockito.when(orderCrudRepository.findById(ID)).thenThrow(DatabaseUnavailable.class);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> orderRepository.find(ID));

        Mockito.verify(orderCrudRepository).findById(ID);
    }

    @Test
    void saveSuccess() {
        Mockito.when(orderCrudRepository.save(ORDER_WO_ID)).thenReturn(ORDER_WITH_ID);

        Truth.assertThat(orderRepository.save(ORDER_WO_ID)).isEqualTo(ORDER_WITH_ID);

        Mockito.verify(orderCrudRepository).save(ORDER_WO_ID);
    }

    @Test
    void saveDatabaseUnavailable() {
        Mockito.when(orderCrudRepository.save(ORDER_WO_ID)).thenThrow(DatabaseUnavailable.class);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> orderRepository.save(ORDER_WO_ID));

        Mockito.verify(orderCrudRepository).save(ORDER_WO_ID);
    }

    @Test
    void updateSuccess() {
        Mockito.when(orderCrudRepository.save(ORDER_WITH_ID)).thenReturn(ORDER_WITH_ID);

        Truth.assertThat(orderRepository.update(ORDER_WITH_ID)).isEqualTo(ORDER_WITH_ID);

        Mockito.verify(orderCrudRepository).save(ORDER_WITH_ID);
    }

    @Test
    void updateDatabaseUnavailable() {
        Mockito.when(orderCrudRepository.save(ORDER_WITH_ID)).thenThrow(DatabaseUnavailable.class);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> orderRepository.update(ORDER_WITH_ID));

        Mockito.verify(orderCrudRepository).save(ORDER_WITH_ID);
    }

    @Test
    void deleteSuccess() {
        orderRepository.delete(ID);

        Mockito.verify(orderCrudRepository).deleteById(ID);
    }

    @Test
    void deleteDatabaseUnavailable() {
        Mockito.doThrow(DatabaseUnavailable.class).when(orderCrudRepository).deleteById(ID);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> orderRepository.delete(ID));

        Mockito.verify(orderCrudRepository).deleteById(ID);
    }
}