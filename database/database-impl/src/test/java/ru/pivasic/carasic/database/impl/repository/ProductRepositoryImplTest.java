package ru.pivasic.carasic.database.impl.repository;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import ru.pivasic.carasic.database.api.exception.DatabaseUnavailable;
import ru.pivasic.carasic.database.api.model.Product;
import ru.pivasic.carasic.database.api.repository.ProductRepository;
import ru.pivasic.carasic.database.impl.repository.spring.jpa.ProductPagingRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author TorstendasTost
 * @since 05.08.2022
 */
class ProductRepositoryImplTest {

    private static final Long ID = 1L;

    private static final Integer PAGE = 1;

    private static final Integer SIZE = 2;

    private static final Product PRODUCT_WITH_ID = Product.builder().id(ID).build();

    private static final Product PRODUCT_WO_ID = Product.builder().build();

    private static final List<Product> PRODUCT_LIST = List.of(PRODUCT_WITH_ID);

    private static final Optional<Product> OPTIONAL_PRODUCT = Optional.of(PRODUCT_WITH_ID);

    private static final PageRequest PAGE_REQUEST = PageRequest.of(PAGE, SIZE);

    private Page productPage;

    private ProductPagingRepository productPagingRepository;

    private ProductRepository productRepository;

    @BeforeEach
    void init() {
        productPage = Mockito.mock(Page.class);
        productPagingRepository = Mockito.mock(ProductPagingRepository.class);
        productRepository = new ProductRepositoryImpl(productPagingRepository);
    }

    @Test
    void findSuccess() {
        Mockito.when(productPagingRepository.findById(ID)).thenReturn(OPTIONAL_PRODUCT);

        Truth.assertThat(productRepository.find(ID)).isEqualTo(OPTIONAL_PRODUCT);

        Mockito.verify(productPagingRepository).findById(ID);
    }

    @Test
    void findDatabaseUnavailable() {
        Mockito.when(productPagingRepository.findById(ID)).thenThrow(DatabaseUnavailable.class);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> productRepository.find(ID));

        Mockito.verify(productPagingRepository).findById(ID);
    }

    @Test
    void findAllSuccess() {
        Mockito.when(productPagingRepository.findAll(PAGE_REQUEST)).thenReturn(productPage);
        Mockito.when(productPage.toList()).thenReturn(PRODUCT_LIST);

        Truth.assertThat(productRepository.findAll(PAGE, SIZE)).isEqualTo(PRODUCT_LIST);

        Mockito.verify(productPagingRepository).findAll(PAGE_REQUEST);
    }

    @Test
    void findAllDatabaseUnavailable() {
        Mockito.when(productPagingRepository.findAll(PAGE_REQUEST)).thenThrow(DatabaseUnavailable.class);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> productRepository.findAll(PAGE, SIZE));

        Mockito.verify(productPagingRepository).findAll(PAGE_REQUEST);
    }

    @Test
    void saveSuccess() {
        Mockito.when(productPagingRepository.save(PRODUCT_WO_ID)).thenReturn(PRODUCT_WITH_ID);

        Truth.assertThat(productRepository.save(PRODUCT_WO_ID)).isEqualTo(PRODUCT_WITH_ID);

        Mockito.verify(productPagingRepository).save(PRODUCT_WO_ID);
    }

    @Test
    void saveDatabaseUnavailable() {
        Mockito.when(productPagingRepository.save(PRODUCT_WO_ID)).thenThrow(DatabaseUnavailable.class);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> productRepository.save(PRODUCT_WO_ID));

        Mockito.verify(productPagingRepository).save(PRODUCT_WO_ID);
    }

    @Test
    void updateSuccess() {
        Mockito.when(productPagingRepository.save(PRODUCT_WITH_ID)).thenReturn(PRODUCT_WITH_ID);

        Truth.assertThat(productRepository.save(PRODUCT_WITH_ID)).isEqualTo(PRODUCT_WITH_ID);

        Mockito.verify(productPagingRepository).save(PRODUCT_WITH_ID);
    }

    @Test
    void updateDatabaseUnavailable() {
        Mockito.when(productPagingRepository.save(PRODUCT_WITH_ID)).thenThrow(DatabaseUnavailable.class);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> productRepository.save(PRODUCT_WITH_ID));

        Mockito.verify(productPagingRepository).save(PRODUCT_WITH_ID);
    }

    @Test
    void deleteSuccess() {
        productRepository.delete(ID);

        Mockito.verify(productPagingRepository).deleteById(ID);
    }

    @Test
    void deleteDatabaseUnavailable() {
        Mockito.doThrow(DatabaseUnavailable.class).when(productPagingRepository).deleteById(ID);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> productRepository.delete(ID));

        Mockito.verify(productPagingRepository).deleteById(ID);
    }
}