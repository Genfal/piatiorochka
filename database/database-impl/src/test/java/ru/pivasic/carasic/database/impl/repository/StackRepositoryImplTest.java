package ru.pivasic.carasic.database.impl.repository;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.pivasic.carasic.database.api.exception.DatabaseUnavailable;
import ru.pivasic.carasic.database.api.model.Stack;
import ru.pivasic.carasic.database.api.repository.StackRepository;
import ru.pivasic.carasic.database.impl.repository.spring.jpa.StackCrudRepository;

import java.util.Optional;

/**
 * @author TorstendasTost
 * @since 04.08.2022
 */
class StackRepositoryImplTest {

    private static final Long ID = 1L;

    private static final Stack STACK_WITH_ID = Stack.builder().id(ID).build();

    private static final Stack STACK_WO_ID = Stack.builder().build();

    private static final Optional<Stack> OPTIONAL_STACK = Optional.of(STACK_WITH_ID);

    private StackCrudRepository stackCrudRepository;

    private StackRepository stackRepository;

    @BeforeEach
    void init() {
        stackCrudRepository = Mockito.mock(StackCrudRepository.class);
        stackRepository = new StackRepositoryImpl(stackCrudRepository);
    }

    @Test
    void findSuccess() {
        Mockito.when(stackCrudRepository.findById(ID)).thenReturn(OPTIONAL_STACK);

        Truth.assertThat(stackRepository.find(ID)).isEqualTo(OPTIONAL_STACK);

        Mockito.verify(stackCrudRepository).findById(ID);
    }

    @Test
    void findDatabaseUnavailable() {
        Mockito.when(stackCrudRepository.findById(ID)).thenThrow(DatabaseUnavailable.class);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> stackRepository.find(ID));

        Mockito.verify(stackCrudRepository).findById(ID);
    }

    @Test
    void save() {
        Mockito.when(stackCrudRepository.save(STACK_WO_ID)).thenReturn(STACK_WITH_ID);

        Truth.assertThat(stackRepository.save(STACK_WO_ID)).isEqualTo(STACK_WITH_ID);

        Mockito.verify(stackCrudRepository).save(STACK_WO_ID);
    }

    @Test
    void saveDatabaseUnavailable() {
        Mockito.when(stackCrudRepository.save(STACK_WO_ID)).thenThrow(DatabaseUnavailable.class);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> stackRepository.save(STACK_WO_ID));

        Mockito.verify(stackCrudRepository).save(STACK_WO_ID);
    }

    @Test
    void updateSuccess() {
        Mockito.when(stackCrudRepository.save(STACK_WITH_ID)).thenReturn(STACK_WITH_ID);

        Truth.assertThat(stackRepository.update(STACK_WITH_ID)).isEqualTo(STACK_WITH_ID);

        Mockito.verify(stackCrudRepository).save(STACK_WITH_ID);
    }

    @Test
    void updateDatabaseUnavailable() {
        Mockito.when(stackCrudRepository.save(STACK_WITH_ID)).thenThrow(DatabaseUnavailable.class);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> stackRepository.update(STACK_WITH_ID));

        Mockito.verify(stackCrudRepository).save(STACK_WITH_ID);
    }

    @Test
    void deleteSuccess() {
        stackRepository.delete(ID);

        Mockito.verify(stackCrudRepository).deleteById(ID);
    }

    @Test
    void deleteDatabaseUnavailable() {
        Mockito.doThrow(DatabaseUnavailable.class).when(stackCrudRepository).deleteById(ID);

        Assertions.assertThrows(DatabaseUnavailable.class, () -> stackRepository.delete(ID));

        Mockito.verify(stackCrudRepository).deleteById(ID);
    }
}