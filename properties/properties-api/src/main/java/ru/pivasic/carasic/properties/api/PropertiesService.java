package ru.pivasic.carasic.properties.api;

import lombok.NonNull;

/**
 * Сервис для пропертей
 *
 * @author Viktor Konev
 * @since 05.08.2022
 */
public interface PropertiesService {
    /**
     * Получение значения по ключу
     *
     * @param key по которому будет найдено значение
     * @return {@link String} значения
     */
    String getValue(@NonNull String key);

    /**
     * Получение значений массива по ключу
     *
     * @param key по которому будет найдено значение
     * @return {@link String[]} значения
     */
    String[] getValueArray(@NonNull String key);

    /**
     * Запись изменённой проперти
     */
    void addPropertiesChanged();
}
