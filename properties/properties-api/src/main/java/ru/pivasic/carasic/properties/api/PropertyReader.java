package ru.pivasic.carasic.properties.api;

import java.util.List;

/**
 * Интерфейс для получения значений из файла конфигурации
 * @param <T> Класс получаемого объекта
 *
 * @author TorstendasTost
 * @since 29.08.2022
 */
public interface PropertyReader<T> {

    /**
     * Метод для получения значения из файла конфигурации
     *
     * @return Объект нужного класса
     */
    T get();

    /**
     * Метод для получения значений из файла конфигурации
     *
     * @return {@link List} со значениями нужного класса
     */
    List<T> getList();
}
