package ru.pivasic.carasic.properties.api.annotation;

/**
 * Enum содержащий ключ и класс этого ключа
 *
 * @author TorstendasTost
 * @since 19.08.2022
 */
public enum ConfigurationEnum {
    FILES_SIZE("files.size", Integer.class),
    FILES_EXTENSIONS("files.extensions", String[].class);

    private final String key;

    private final Class<?> value;

    ConfigurationEnum(String key, Class<?> value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public <T> Class<T> getClazz() {
        return (Class<T>) value;
    }
}
