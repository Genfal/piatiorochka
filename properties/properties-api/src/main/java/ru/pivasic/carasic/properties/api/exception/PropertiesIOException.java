package ru.pivasic.carasic.properties.api.exception;

/**
 * Ошибка файла конфигурации
 *
 * @author TorstendasTost
 * @since 31.08.2022
 */
public class PropertiesIOException extends RuntimeException {
    public PropertiesIOException(String message) {
        super(message);
    }
}
