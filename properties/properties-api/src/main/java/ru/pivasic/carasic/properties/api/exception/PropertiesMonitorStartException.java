package ru.pivasic.carasic.properties.api.exception;

/**
 * Класс ошибки старта монитора проперти
 *
 * @author Viktor Konev
 * @since 17.08.2022
 */
public class PropertiesMonitorStartException extends RuntimeException {
    public PropertiesMonitorStartException(String message) {
        super(message);
    }
}
