package ru.pivasic.carasic.properties.api.exception;

/**
 * Класс ошибки при не найденном файле проперти
 *
 * @author Viktor Konev
 * @since 17.08.2022
 */
public class PropertiesNotFoundException extends RuntimeException {
    public PropertiesNotFoundException(String message) {
        super(message);
    }
}
