package ru.pivasic.carasic.properties.api.exception;

/**
 * Класс ошибки чтения проперти
 *
 * @author Viktor Konev
 * @since 17.08.2022
 */
public class PropertiesReadException extends RuntimeException{
    public PropertiesReadException(String message) {
        super(message);
    }
}
