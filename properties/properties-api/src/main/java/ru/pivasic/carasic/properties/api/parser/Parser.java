package ru.pivasic.carasic.properties.api.parser;

/**
 * Интерфейс для парсинга {@link String} в другой класс
 * @param <T> Класс в который будет запаршена {@link String}
 *
 * @author TorstendasTost
 * @since 24.08.2022
 */
public interface Parser<T> {

    /**
     * Метод который парсит {@link String} в другой класс
     *
     * @param value {@link String} которая будет парситься
     * @return Резултат парсинга в {@link T}
     */
    T parse(String value);

    /**
     * Метод который парсит {@link String[]} в другой класс
     *
     * @param value {@link String[]} которая будет парситься
     * @return Резултат парсинга в {@link T}
     */
    T[] parseArray(String[] value);
}
