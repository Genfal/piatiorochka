package ru.pivasic.carasic.properties.api.parser;

import ru.pivasic.carasic.properties.api.PropertyReader;

/**
 * Интерфейс для создания {@link PropertyReader}
 *
 * @author TorstendasTost
 * @since 24.08.2022
 */
public interface PropertyReaderFactory {

    /**
     * Метод для создания {@link PropertyReader}
     *
     * @param key значение которого будет получать {@link PropertyReader}
     * @param clazz класс возвращаемого объекта
     * @return {@link PropertyReader} для переданного ключа и класса
     */
    <T> PropertyReader<T> create(String key, Class<T> clazz);
}
