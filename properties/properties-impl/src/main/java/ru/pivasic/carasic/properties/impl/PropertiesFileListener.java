package ru.pivasic.carasic.properties.impl;

import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import ru.pivasic.carasic.properties.api.PropertiesService;

import java.io.File;

/**
 * Прослушивание файла с параметрами
 *
 * @author Viktor Konev
 * @since 10.08.2022
 */
public class PropertiesFileListener extends FileAlterationListenerAdaptor {

    private final PropertiesService propertiesService;

    public PropertiesFileListener(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    @Override
    public void onFileChange(File file) {
        propertiesService.addPropertiesChanged();
    }
}
