package ru.pivasic.carasic.properties.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration2.YAMLConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import ru.pivasic.carasic.properties.api.PropertiesService;
import ru.pivasic.carasic.properties.api.exception.PropertiesIOException;
import ru.pivasic.carasic.properties.api.exception.PropertiesReadException;

import javax.annotation.Nullable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Запись изменённой проперти
 *
 * @author Viktor Konev
 * @since 04.08.2022
 */
@Slf4j
public class PropertiesServiceImpl implements PropertiesService {

    /**
     * Мапа для хранения значений проперти
     */
    private final HashMap<String, String[]> propertyMap;

    private final String propertiesPath;

    private final YAMLConfiguration yamlConfiguration;

    public PropertiesServiceImpl(HashMap<String, String[]> propertyMap, String propertiesPath) {
        this.propertyMap = propertyMap;
        this.propertiesPath = propertiesPath;
        yamlConfiguration = new YAMLConfiguration();
    }

    public PropertiesServiceImpl(String propertiesPath) {
        this(new HashMap<>(), propertiesPath);
    }

    @Override
    public void addPropertiesChanged() {
        try (InputStream inputStream = new FileInputStream(propertiesPath)) {
            yamlConfiguration.read(inputStream);

            Iterator<String> it = yamlConfiguration.getKeys();
            while (it.hasNext()) {
                String key = it.next();
                String[] value = yamlConfiguration.getStringArray(key);
                propertyMap.put(key, value);
                log.info("Ключ - {}, значение - {}", key, Arrays.toString(value));
            }
        } catch (ConfigurationException e) {
            throw new PropertiesReadException("Ошибка в файле конфигурации");
        } catch (IOException e) {
            throw new PropertiesIOException("Ошибка файла конфигурации");
        }
    }

    @Nullable
    @Override
    public String getValue(@NonNull String key) {
        return propertyMap.get(key)[0];
    }

    @Nullable
    @Override
    public String[] getValueArray(@NonNull String key) {
        return propertyMap.get(key);
    }
}
