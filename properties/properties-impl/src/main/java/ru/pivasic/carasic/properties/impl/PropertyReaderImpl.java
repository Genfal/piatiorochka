package ru.pivasic.carasic.properties.impl;

import ru.pivasic.carasic.properties.api.PropertiesService;
import ru.pivasic.carasic.properties.api.PropertyReader;
import ru.pivasic.carasic.properties.api.parser.Parser;

import java.util.Arrays;
import java.util.List;

/**
 * Класс реализации {@link PropertyReader}
 *
 * @author TorstendasTost
 * @since 29.08.2022
 */
public class PropertyReaderImpl<T> implements PropertyReader<T> {

    private final PropertiesService propertiesService;
    private final Parser<T> parser;
    private final String key;

    public PropertyReaderImpl(PropertiesService propertiesService, Parser<T> parser, String key) {
        this.propertiesService = propertiesService;
        this.parser = parser;
        this.key = key;
    }

    public T get() {
        String value = propertiesService.getValue(key);
        return parser.parse(value);
    }

    public List<T> getList() {
        String[] values = propertiesService.getValueArray(key);
        return Arrays.asList(parser.parseArray(values));
    }
}
