package ru.pivasic.carasic.properties.impl.daemon;

import lombok.NonNull;

import java.util.concurrent.ThreadFactory;

/**
 * Класс для создания потока демона
 *
 * @author Viktor Konev
 * @since 12.08.2022
 */
public class DaemonThreadFactory implements ThreadFactory {

    @Override
    public Thread newThread(@NonNull Runnable r) {
        Thread thread = new Thread(r);
        thread.setDaemon(true);
        return thread;
    }
}
