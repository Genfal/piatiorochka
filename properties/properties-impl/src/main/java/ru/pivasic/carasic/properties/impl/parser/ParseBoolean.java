package ru.pivasic.carasic.properties.impl.parser;

import ru.pivasic.carasic.properties.api.parser.Parser;

import java.util.Arrays;

/**
 * Класс для парсинга значений в {@link Boolean}
 *
 * @author TorstendasTost
 * @since 24.08.2022
 */
public class ParseBoolean implements Parser<Boolean> {

    @Override
    public Boolean parse(String value) {
        return Boolean.parseBoolean(value);
    }

    @Override
    public Boolean[] parseArray(String[] value) {
        return Arrays.stream(value).map(Boolean::parseBoolean).toArray(Boolean[]::new);
    }
}
