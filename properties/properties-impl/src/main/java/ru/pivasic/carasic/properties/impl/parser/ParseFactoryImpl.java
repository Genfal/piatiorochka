package ru.pivasic.carasic.properties.impl.parser;

import ru.pivasic.carasic.properties.api.PropertiesService;
import ru.pivasic.carasic.properties.api.PropertyReader;
import ru.pivasic.carasic.properties.api.parser.Parser;
import ru.pivasic.carasic.properties.api.parser.PropertyReaderFactory;
import ru.pivasic.carasic.properties.impl.PropertyReaderImpl;

import java.util.HashMap;

/**
 * Класс реализации {@link PropertyReaderFactory}
 *
 * @author TorstendasTost
 * @since 24.08.2022
 */
public class ParseFactoryImpl implements PropertyReaderFactory {

    private PropertiesService propertiesService;
    private HashMap<Class<?>, Parser<?>> parserHashMap = new HashMap<>();

    public ParseFactoryImpl(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    /**
     * Метод для инициализации parserHashMap
     */
    public void init() {
        parserHashMap.put(Integer.class, new ParseInteger());
        parserHashMap.put(String.class, new ParseString());
        parserHashMap.put(Boolean.class, new ParseBoolean());
        parserHashMap.put(Integer[].class, new ParseInteger());
        parserHashMap.put(String[].class, new ParseString());
        parserHashMap.put(Boolean[].class, new ParseBoolean());
    }

    public <T> PropertyReader<T> create(String key, Class<T> clazz) {
        return new PropertyReaderImpl<>(propertiesService, getParser(clazz), key);
    }

    /**
     * Метод для получения парсера нужного класса
     *
     * @param tClass определяющий {@link Parser}
     * @return {@link Parser} нужного класса
     */
    public <T> Parser<T> getParser(Class<T> tClass) {
        return (Parser<T>) parserHashMap.get(tClass);
    }
}
