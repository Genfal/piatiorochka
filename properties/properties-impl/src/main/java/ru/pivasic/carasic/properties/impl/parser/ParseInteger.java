package ru.pivasic.carasic.properties.impl.parser;

import ru.pivasic.carasic.properties.api.parser.Parser;

import java.util.Arrays;

/**
 * Класс для парсинга значений в {@link Integer}
 *
 * @author TorstendasTost
 * @since 24.08.2022
 */
public class ParseInteger implements Parser<Integer> {
    @Override
    public Integer parse(String value) {
        return Integer.parseInt(value);
    }

    @Override
    public Integer[] parseArray(String[] value) {
        return Arrays.stream(value).map(Integer::parseInt).toArray(Integer[]::new);
    }
}
