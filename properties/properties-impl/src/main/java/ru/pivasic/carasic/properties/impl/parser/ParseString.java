package ru.pivasic.carasic.properties.impl.parser;

import ru.pivasic.carasic.properties.api.parser.Parser;

/**
 * Класс для парсинга значений в {@link String}
 *
 * @author TorstendasTost
 * @since 24.08.2022
 */
public class ParseString implements Parser<String> {
    @Override
    public String parse(String value) {
        return value;
    }

    @Override
    public String[] parseArray(String[] value) {
        return value;
    }
}
