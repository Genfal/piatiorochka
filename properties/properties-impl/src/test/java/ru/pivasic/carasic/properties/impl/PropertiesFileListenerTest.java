package ru.pivasic.carasic.properties.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.pivasic.carasic.properties.api.PropertiesService;

import java.io.File;

/**
 * @author Viktor Konev
 * @since 13.08.2022
 */
class PropertiesFileListenerTest {

    private PropertiesService propertiesService;

    private PropertiesFileListener propertiesFileListener;

    private File file;

    @BeforeEach
    void init() {
        file = Mockito.mock(File.class);
        propertiesService = Mockito.mock(PropertiesService.class);
        propertiesFileListener = new PropertiesFileListener(propertiesService);
    }

    @Test
    void onFileChange() {
        propertiesFileListener.onFileChange(file);

        Mockito.verify(propertiesService).addPropertiesChanged();
    }
}
