package ru.pivasic.carasic.properties.impl;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import ru.pivasic.carasic.properties.api.exception.PropertiesIOException;
import ru.pivasic.carasic.properties.api.exception.PropertiesReadException;

import java.util.HashMap;

/**
 * @author Viktor Konev
 * @since 12.08.2022
 */
class PropertiesServiceImplTest {

    private static final String PROPERTY_KEY = "login";

    private static final String PROPERTY_VALUE_ADMIN = "admin";

    private static final String[] PROPERTY_VALUE_ADMIN_ARRAY = {PROPERTY_VALUE_ADMIN};

    private static final String PROPERTY_KEY_PASSWORD = "password";

    private static final String PROPERTY_VALUE_PASSWORD = "adm";

    private static final String[] PROPERTY_VALUE_PASSWORD_ARRAY = {PROPERTY_VALUE_PASSWORD};

    private static final String PROPERTY_VALUE = "vins.conev";

    private static final String[] PROPERTY_VALUE_ARRAY = {PROPERTY_VALUE};

    private static HashMap<String, String[]> propertyMap;

    private static AutoCloseable closeable;

    @BeforeEach
    void initPropertiesServiceImpl() {
        closeable = MockitoAnnotations.openMocks(this);
        propertyMap = new HashMap<>();
    }

    @AfterEach
    void closePropertiesServiceImpl() throws Exception {
        closeable.close();
    }

    @Test
    void addPropertiesChanged() {
        PropertiesServiceImpl propertiesService = new PropertiesServiceImpl(propertyMap, "src/test/resources/config.yaml");

        propertiesService.addPropertiesChanged();

        Truth.assertThat(propertyMap.get(PROPERTY_KEY)).isEqualTo(PROPERTY_VALUE_ADMIN_ARRAY);
        Truth.assertThat(propertyMap.get(PROPERTY_KEY_PASSWORD)).isEqualTo(PROPERTY_VALUE_PASSWORD_ARRAY);
    }

    @Test
    void addPropertiesChangedIOException() {
        PropertiesServiceImpl propertiesService = new PropertiesServiceImpl(propertyMap, "src/test/resources/foo.properties");

        Assertions.assertThrows(PropertiesIOException.class, propertiesService::addPropertiesChanged);
    }

    @Test
    void addPropertiesChangedReadException() {
        PropertiesServiceImpl propertiesService = new PropertiesServiceImpl(propertyMap, "src/test/resources/read-exception.yaml");

        Assertions.assertThrows(PropertiesReadException.class, propertiesService::addPropertiesChanged);
    }

    @Test
    void getValue() {
        propertyMap.put(PROPERTY_KEY, PROPERTY_VALUE_ARRAY);
        PropertiesServiceImpl propertiesService = new PropertiesServiceImpl(propertyMap, "src/test/resources/config.yaml");

        Truth.assertThat(propertiesService.getValue(PROPERTY_KEY)).isEqualTo(PROPERTY_VALUE);
    }

    @Test
    void getValueArray() {
        propertyMap.put(PROPERTY_KEY, PROPERTY_VALUE_ARRAY);
        PropertiesServiceImpl propertiesService = new PropertiesServiceImpl(propertyMap, "src/test/resources/config.yaml");

        Truth.assertThat(propertiesService.getValueArray(PROPERTY_KEY)).isEqualTo(PROPERTY_VALUE_ARRAY);
    }
}
