package ru.pivasic.carasic.properties.impl;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.pivasic.carasic.properties.api.PropertiesService;
import ru.pivasic.carasic.properties.api.parser.Parser;
import ru.pivasic.carasic.properties.impl.parser.ParseInteger;

import java.util.List;

/**
 * @author TorstendasTost
 * @since 03.09.2022
 */
@ExtendWith({MockitoExtension.class})
class PropertyReaderImplTest {

    private static final String KEY = "key";

    private static final String VALUE = "123";

    private static final Integer EXPECTED_VALUE = 123;

    private static final String[] VALUE_ARRAY = {"123", "321"};

    private static final List<Integer> EXPECTED_VALUE_LIST = List.of(123, 321);

    @Mock
    private PropertiesService propertiesService;

    @InjectMocks
    private PropertyReaderImpl<Integer> propertyReader;

    @BeforeEach
    void setUp() {
        Parser<Integer> parser = new ParseInteger();
        propertyReader = new PropertyReaderImpl<>(propertiesService, parser, KEY);
    }

    @Test
    void get() {
        Mockito.when(propertiesService.getValue(KEY)).thenReturn(VALUE);
        Truth.assertThat(propertyReader.get()).isEqualTo(EXPECTED_VALUE);
    }

    @Test
    void getList() {
        Mockito.when(propertiesService.getValueArray(KEY)).thenReturn(VALUE_ARRAY);
        Truth.assertThat(propertyReader.getList()).isEqualTo(EXPECTED_VALUE_LIST);
    }
}