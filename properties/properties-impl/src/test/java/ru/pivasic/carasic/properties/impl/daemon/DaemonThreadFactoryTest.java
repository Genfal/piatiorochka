package ru.pivasic.carasic.properties.impl.daemon;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/**
 * @author Viktor Konev
 * @since 15.08.2022
 */
class DaemonThreadFactoryTest {

    private DaemonThreadFactory daemonThreadFactory;

    private Runnable runnable;

    @BeforeEach
    void init() {
        runnable = Mockito.mock(Runnable.class);
        daemonThreadFactory = new DaemonThreadFactory();
    }

    @Test
    void newThread() {
        Truth.assertThat(daemonThreadFactory.newThread(runnable).isDaemon()).isTrue();
    }
}