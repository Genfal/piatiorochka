package ru.pivasic.carasic.properties.impl.parser;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.pivasic.carasic.properties.api.parser.Parser;

/**
 * @author TorstendasTost
 * @since 03.09.2022
 */
abstract class BaseParseTest<T> {

    private String configValue;

    private T expectedValue;

    private String[] configValueArray;

    private T[] expectedValueArray;

    private Parser<T> parser;

    protected abstract String configValue();

    protected abstract T expectedValue();

    protected abstract String[] configValueArray();

    protected abstract T[] expectedValueArray();

    protected abstract Parser<T> parser();

    @BeforeEach
    protected void setUp() {
        configValue = configValue();
        expectedValue = expectedValue();
        configValueArray = configValueArray();
        expectedValueArray = expectedValueArray();
        parser = parser();
    }

    @Test
    void parse() {
        Truth.assertThat(parser.parse(configValue)).isEqualTo(expectedValue);
    }

    @Test
    void parseArray() {
        Truth.assertThat(parser.parseArray(configValueArray)).isEqualTo(expectedValueArray);
    }
}