package ru.pivasic.carasic.properties.impl.parser;

import ru.pivasic.carasic.properties.api.parser.Parser;

/**
 * @author TorstendasTost
 * @since 02.09.2022
 */
class ParseBooleanTest extends BaseParseTest<Boolean> {

    @Override
    protected String configValue() {
        return "true";
    }

    @Override
    protected Boolean expectedValue() {
        return true;
    }

    @Override
    protected String[] configValueArray() {
        return new String[]{"true", "true"};
    }

    @Override
    protected Boolean[] expectedValueArray() {
        return new Boolean[]{true, true};
    }

    @Override
    protected Parser<Boolean> parser() {
        return new ParseBoolean();
    }
}