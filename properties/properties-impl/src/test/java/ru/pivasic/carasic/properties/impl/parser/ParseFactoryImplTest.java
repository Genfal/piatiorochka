package ru.pivasic.carasic.properties.impl.parser;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.pivasic.carasic.properties.api.PropertiesService;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @author TorstendasTost
 * @since 02.09.2022
 */
@ExtendWith(MockitoExtension.class)
class ParseFactoryImplTest {

    private static final String KEY = "key";

    private static final Integer INTEGER = 1;

    private static final String STRING = "string";

    private static final Boolean BOOLEAN = true;

    private static final Integer[] INTEGER_ARRAY = {1, 2};

    private static final String[] STRING_ARRAY = {"string", "string"};

    private static final Boolean[] BOOLEAN_ARRAY = {true, false};

    @Mock
    private PropertiesService propertiesService;

    @InjectMocks
    private ParseFactoryImpl parseFactoryImpl;

    @BeforeEach
    void init() {
        parseFactoryImpl.init();
    }

    @ParameterizedTest
    @MethodSource("createTest")
    void create(Class<?> clazz, Object value) {
        Mockito.when(propertiesService.getValue(KEY)).thenReturn(String.valueOf(value));
        Truth.assertThat(parseFactoryImpl.create(KEY, clazz).get()).isEqualTo(value);
    }

    @ParameterizedTest
    @MethodSource("createTestArray")
    void createArray(Class<?> clazz, Object[] value) {
        String[] strings = Arrays.stream(value).map(String::valueOf).toArray(String[]::new);
        Mockito.when(propertiesService.getValueArray(KEY)).thenReturn(strings);
        Truth.assertThat(parseFactoryImpl.create(KEY, clazz).getList()).isEqualTo(Arrays.asList(value));
    }

    static Stream<Arguments> createTest() {
        return Stream.of(
                Arguments.of(Integer.class, INTEGER),
                Arguments.of(String.class, STRING),
                Arguments.of(Boolean.class, BOOLEAN)
        );
    }

    static Stream<Arguments> createTestArray() {
        return Stream.of(
                Arguments.of(Integer[].class, INTEGER_ARRAY),
                Arguments.of(String[].class, STRING_ARRAY),
                Arguments.of(Boolean[].class, BOOLEAN_ARRAY)
        );
    }
}