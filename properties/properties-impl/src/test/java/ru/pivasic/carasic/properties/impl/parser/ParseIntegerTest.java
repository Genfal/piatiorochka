package ru.pivasic.carasic.properties.impl.parser;

import ru.pivasic.carasic.properties.api.parser.Parser;

/**
 * @author TorstendasTost
 * @since 02.09.2022
 */
class ParseIntegerTest extends BaseParseTest<Integer> {

    @Override
    protected String configValue() {
        return "123";
    }

    @Override
    protected Integer expectedValue() {
        return 123;
    }

    @Override
    protected String[] configValueArray() {
        return new String[]{"123", "321"};
    }

    @Override
    protected Integer[] expectedValueArray() {
        return new Integer[]{123, 321};
    }

    @Override
    protected Parser<Integer> parser() {
        return new ParseInteger();
    }
}