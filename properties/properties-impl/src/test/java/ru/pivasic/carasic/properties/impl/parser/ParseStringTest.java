package ru.pivasic.carasic.properties.impl.parser;

import ru.pivasic.carasic.properties.api.parser.Parser;

/**
 * @author TorstendasTost
 * @since 02.09.2022
 */
class ParseStringTest extends BaseParseTest<String> {

    @Override
    protected String configValue() {
        return "string";
    }

    @Override
    protected String expectedValue() {
        return "string";
    }

    @Override
    protected String[] configValueArray() {
        return new String[]{"string", "string"};
    }

    @Override
    protected String[] expectedValueArray() {
        return new String[]{"string", "string"};
    }

    @Override
    protected Parser<String> parser() {
        return new ParseString();
    }
}