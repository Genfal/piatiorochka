package ru.pivasic.carasic.service.api;

import lombok.NonNull;
import ru.pivasic.carasic.database.api.model.Customer;

/**
 * Интерфейс для работы с {@link Customer}
 *
 * @author Viktor Konev
 * @since 22.07.2022
 */
public interface CustomerService {

    /**
     * Сохранение {@link Customer}
     *
     * @param customer {@link Customer} который будет сохранён
     * @return Полученного {@link Customer} c id
     */
    Customer save(@NonNull Customer customer);

    /**
     * Получение {@link Customer}
     *
     * @param id Идентификатор по которому будет найден {@link Customer}
     * @return {@link Customer} который был получен по идентификатору
     */
    Customer get(@NonNull Long id);

    /**
     * Обновление {@link Customer}
     *
     * @param customer {@link Customer} который будет обновлён
     * @return Обновлённого {@link Customer}
     */
    Customer update(@NonNull Customer customer);

    /**
     * Удаление {@link Customer}
     *
     * @param id Идентификатор по которому будет удалён {@link Customer}
     */
    void delete(@NonNull Long id);
}