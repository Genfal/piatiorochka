package ru.pivasic.carasic.service.api;

import lombok.NonNull;
import ru.pivasic.carasic.database.api.exception.order.NullOrderIdException;
import ru.pivasic.carasic.database.api.exception.order.OrderNotFoundException;
import ru.pivasic.carasic.database.api.model.Order;

/**
 * Интерфейс для работы с {@link Order}
 *
 * @author TorstendasTost
 * @since 23.07.2022
 */
public interface OrderService {

    /**
     * Сохранение {@link Order}
     *
     * @param order {@link Order} который будет сохранён
     * @return Полученный {@link Order} с id
     */
    Order save(@NonNull Order order);

    /**
     * Получение {@link Order}
     *
     * @param id Идентификатор по которому будет найден {@link Order}
     * @return Полученный {@link Order} который был найден по id
     * @throws NullOrderIdException передан {@link Order} без поля id
     * @throws OrderNotFoundException {@link Order} с таким id не найден
     */
    Order get(@NonNull Long id);

    /**
     * Обновление {@link Order}
     *
     * @param order {@link Order} который будет обновлен
     * @return Обновлённый {@link Order}
     * @throws NullOrderIdException передан {@link Order} без поля id
     * @throws OrderNotFoundException {@link Order} с таким id не найден
     */
    Order update(@NonNull Order order);

    /**
     * Удаление {@link Order}
     *
     * @param id Идентификатор по которому {@link Order} будет удалён
     * @throws NullOrderIdException передан {@link Order} без поля id
     * @throws OrderNotFoundException {@link Order} с таким id не найден
     */
    void delete(@NonNull Long id);
}
