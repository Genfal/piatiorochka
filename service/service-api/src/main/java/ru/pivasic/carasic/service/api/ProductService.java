package ru.pivasic.carasic.service.api;

import lombok.NonNull;
import ru.pivasic.carasic.database.api.exception.product.NullProductIdException;
import ru.pivasic.carasic.database.api.exception.product.ProductNotFoundException;
import ru.pivasic.carasic.database.api.model.Product;

import java.io.InputStream;
import java.util.List;

/**
 * Интерфейс для работы с {@link Product}
 *
 * @author TorstendasTost
 * @since 23.07.2022
 */
public interface ProductService {

    /**
     * Сохранение {@link Product}
     *
     * @param product {@link Product} который будет сохранён
     * @return Полученный {@link Product} с id
     */
    Product save(@NonNull Product product);

    /**
     * Получение {@link Product}
     *
     * @param id Идентификатор по которому будет найден {@link Product}
     * @return Полученный {@link Product} который был найден по id
     * @throws NullProductIdException передан {@link Product} без поля id
     * @throws ProductNotFoundException {@link Product} с таким id не найден
     */
    Product get(@NonNull Long id);

    /**
     * Получение всех {@link Product} с нумерацией страниц
     *
     * @param page Номер страницы
     * @param size Количество {@link Product} на странице
     * @return {@link List <Product>} находящийся на переданной странице
     */
    List<Product> getAll(@NonNull Integer page, @NonNull Integer size);

    /**
     * Обновление {@link Product}
     *
     * @param product {@link Product} который будет обновлён
     * @return Обновлённый {@link Product}
     * @throws NullProductIdException передан {@link Product} без поля id
     * @throws ProductNotFoundException {@link Product} с таким id не найден
     */
    Product update(@NonNull Product product);

    /**
     * Удаление {@link Product}
     *
     * @param id Идентификатор по которому {@link Product} будет удалён
     * @throws NullProductIdException передан {@link Product} без поля id
     * @throws ProductNotFoundException {@link Product} с таким id не найден
     */
    void delete(@NonNull Long id);

    /**
     * Сохранение изображения {@link Product}
     *
     * @param id Идентификатор по которому будет найден {@link Product}
     * @param picture файл который будет нарезан на соответсвующие размеры
     */
    void savePicture(@NonNull Long id, @NonNull InputStream picture);

    /**
     * Удаление изображения {@link Product}
     *
     * @param id Идентификатор по которому изображение будет удалено
     */
    void deletePicture(@NonNull Long id);
}
