package ru.pivasic.carasic.service.api;

import lombok.NonNull;
import ru.pivasic.carasic.database.api.exception.stack.NullStackIdException;
import ru.pivasic.carasic.database.api.exception.stack.StackNotFoundException;
import ru.pivasic.carasic.database.api.model.Stack;

/**
 * Интерфейс для работы с {@link Stack}
 *
 * @author TorstendasTost
 * @since 23.07.2022
 */
public interface StackService {

    /**
     * Сохранение {@link Stack}
     *
     * @param stack {@link Stack} который будет сохранён
     * @return Полученный {@link Stack} с id
     */
    Stack save(@NonNull Stack stack);

    /**
     * Получение {@link Stack}
     *
     * @param id Идентификатор по которому будет найден {@link Stack}
     * @return Полученный {@link Stack} который был найден по id
     * @throws NullStackIdException передан {@link Stack} без поля id
     * @throws StackNotFoundException {@link Stack} с таким id не найден
     */
    Stack get(@NonNull Long id);

    /**
     * Обновление {@link Stack}
     *
     * @param stack {@link Stack} который будет обновлён
     * @return Обновлённый {@link Stack}
     * @throws NullStackIdException передан {@link Stack} без поля id
     * @throws StackNotFoundException {@link Stack} с таким id не найден
     */
    Stack update(@NonNull Stack stack);

    /**
     * Удаление {@link Stack}
     *
     * @param id Идентификатор по которому {@link Stack} будет удалён
     * @throws NullStackIdException передан {@link Stack} без поля id
     * @throws StackNotFoundException {@link Stack} с таким id не найден
     */
    void delete(@NonNull Long id);
}
