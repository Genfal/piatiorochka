package ru.pivasic.carasic.service.api;

import lombok.NonNull;
import ru.pivasic.carasic.database.api.model.User;

import java.io.InputStream;

/**
 * Интерфейс для работы с {@link User}
 *
 * @author Viktor Konev
 * @since 23.07.2022
 */
public interface UserService {

    /**
     * Сохранение {@link User}
     *
     * @param user {@link User} который будет сохранён
     * @return Полученного {@link User} с id
     */
    User save(@NonNull User user);

    /**
     * Получение {@link User}
     *
     * @param id Идентификатор по которому будет найдет {@link User}
     * @return Полученного {@link User} который был найден по идентификатору
     */
    User get(@NonNull Long id);

    /**
     * Обновление {@link User}
     *
     * @param user {@link User} который будет обновлён
     * @return Обновлённого {@link User}
     */
    User update(@NonNull User user);

    /**
     * Удаление {@link User}
     *
     * @param id Идентификатор по которому будет удалён {@link User}
     */
    void delete(@NonNull Long id);

    /**
     * Верификация {@link User}
     *
     * @param id Идентификатор {@link User} по которму будет произведена верификация
     * @param verificationNumber который прислал {@link User}
     * @return True - верификация пройдена, False - верификация не пройдена
     */
    Boolean verificate(@NonNull Long id,@NonNull String verificationNumber);

    /**
     * Удаление аватара {@link User}
     *
     * @param id {@link User} по которому будет удалён аватар
     */
    void deleteAvatar(@NonNull Long id);

    /**
     * Сохранение аватара {@link User}
     *
     * @param id Идентификатор по которому будет найден {@link User}
     * @param avatar файл который будет нарезан на соответствующие размеры
     */
    void saveAvatar(@NonNull Long id,@NonNull InputStream avatar);
}