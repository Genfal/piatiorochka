package ru.pivasic.carasic.service.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import ru.pivasic.carasic.database.api.exception.order.NullOrderIdException;
import ru.pivasic.carasic.database.api.exception.order.OrderNotFoundException;
import ru.pivasic.carasic.database.api.model.Order;
import ru.pivasic.carasic.database.api.repository.OrderRepository;
import ru.pivasic.carasic.service.api.OrderService;

/**
 * Сервис для работы с {@link Order}
 *
 * @author TorstendasTost
 * @since 31.07.2022
 */
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order save(@NonNull Order order) {
        Order savedOrder = orderRepository.save(order);
        log.info("Заказ с id: {} был сохранён", savedOrder.getId());
        return savedOrder;
    }

    @Override
    public Order get(Long id) {
        if (id == null) {
            throw new NullOrderIdException();
        }
        Order receivedOrder = orderRepository
                .find(id)
                .orElseThrow(OrderNotFoundException::new);
        log.debug("Заказ с id: {} был получен", id);
        return receivedOrder;
    }

    @Override
    public Order update(@NonNull Order order) {
        get(order.getId());
        orderRepository.update(order);
        log.info("Заказ с id: {} был обновлён", order.getId());
        return order;
    }

    @Override
    public void delete(Long id) {
        get(id);
        orderRepository.delete(id);
        log.info("Заказ с id: {} был удалён", id);
    }
}
