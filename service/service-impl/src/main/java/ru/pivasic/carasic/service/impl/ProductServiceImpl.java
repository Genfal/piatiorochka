package ru.pivasic.carasic.service.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import ru.pivasic.carasic.database.api.exception.product.NullProductIdException;
import ru.pivasic.carasic.database.api.exception.product.ProductNotFoundException;
import ru.pivasic.carasic.database.api.model.Product;
import ru.pivasic.carasic.database.api.repository.ProductRepository;
import ru.pivasic.carasic.service.api.ProductService;

import java.io.InputStream;
import java.util.List;

/**
 * Сервис для работы с {@link Product}
 *
 * @author TorstendasTost
 * @since 06.08.2022
 */
@Slf4j
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product save(@NonNull Product product) {
        Product savedProduct = productRepository.save(product);
        log.info("Продукт с id: {} был сохранён", savedProduct.getId());
        return savedProduct;
    }

    @Override
    public Product get(Long id) {
        if (id == null) {
            throw new NullProductIdException();
        }
        Product receivedProduct = productRepository
                .find(id)
                .orElseThrow(ProductNotFoundException::new);
        log.debug("Продукт с id: {} был получен", id);
        return receivedProduct;
    }

    @Override
    public List<Product> getAll(@NonNull Integer page, @NonNull Integer size) {
        List<Product> productList = productRepository.findAll(page, size);
        log.debug("Продукты на странице {} в количестве {} были получены", page, size);
        return productList;
    }

    @Override
    public Product update(@NonNull Product product) {
        get(product.getId());
        productRepository.update(product);
        log.info("Продукт с id: {} был обновлён", product.getId());
        return product;
    }

    @Override
    public void delete(Long id) {
        get(id);
        productRepository.delete(id);
        log.info("Продукт с id: {} был удалён", id);
    }

    @Override
    public void savePicture(@NonNull Long id, @NonNull InputStream picture) {
    }

    @Override
    public void deletePicture(@NonNull Long id) {
    }
}
