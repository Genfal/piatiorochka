package ru.pivasic.carasic.service.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import ru.pivasic.carasic.database.api.exception.stack.NullStackIdException;
import ru.pivasic.carasic.database.api.exception.stack.StackNotFoundException;
import ru.pivasic.carasic.database.api.model.Stack;
import ru.pivasic.carasic.database.api.repository.StackRepository;
import ru.pivasic.carasic.service.api.StackService;

/**
 * Сервис для работы с {@link Stack}
 *
 * @author TorstendasTost
 * @since 04.08.2022
 */
@Slf4j
public class StackServiceImpl implements StackService {

    private final StackRepository stackRepository;

    public StackServiceImpl(StackRepository stackRepository) {
        this.stackRepository = stackRepository;
    }

    @Override
    public Stack save(@NonNull Stack stack) {
        Stack savedStack = stackRepository.save(stack);
        log.debug("Стэк с id: {} был сохранён", stack.getId());
        return savedStack;
    }

    @Override
    public Stack get(Long id) {
        if (id == null) {
            throw new NullStackIdException();
        }
        Stack receivedStack = stackRepository
                .find(id)
                .orElseThrow(StackNotFoundException::new);
        log.debug("Стэк с id: {} был получен", id);
        return receivedStack;
    }

    @Override
    public Stack update(@NonNull Stack stack) {
        get(stack.getId());
        stackRepository.update(stack);
        log.debug("Стэк с id: {} был обновлён", stack.getId());
        return stack;
    }

    @Override
    public void delete(Long id) {
        get(id);
        stackRepository.delete(id);
        log.debug("Стэк с id: {} был удалён", id);
    }
}
