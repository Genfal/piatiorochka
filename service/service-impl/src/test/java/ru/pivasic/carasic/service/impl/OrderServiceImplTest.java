package ru.pivasic.carasic.service.impl;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.pivasic.carasic.database.api.exception.order.NullOrderIdException;
import ru.pivasic.carasic.database.api.exception.order.OrderNotFoundException;
import ru.pivasic.carasic.database.api.model.Order;
import ru.pivasic.carasic.database.api.repository.OrderRepository;
import ru.pivasic.carasic.service.api.OrderService;

import java.util.Optional;

/**
 * @author TorstendasTost
 * @since 01.08.2022
 */
class OrderServiceImplTest {

    private static final Long ID = 1L;

    private static final Order ORDER_WITH_ID = Order.builder().id(ID).build();

    private static final Order ORDER_WO_ID = Order.builder().build();

    private static final Optional<Order> OPTIONAL_ORDER = Optional.of(ORDER_WITH_ID);

    private OrderRepository orderRepository;

    private OrderService orderService;

    @BeforeEach
    void init() {
        orderRepository = Mockito.mock(OrderRepository.class);
        orderService = new OrderServiceImpl(orderRepository);
    }

    @Test
    void getSuccess() {
        Mockito.when(orderRepository.find(ID)).thenReturn(OPTIONAL_ORDER);

        Truth.assertThat(orderService.get(ID)).isEqualTo(ORDER_WITH_ID);

        Mockito.verify(orderRepository).find(ID);
    }

    @Test
    void getNullOrderId() {
        Assertions.assertThrows(NullOrderIdException.class, () -> orderService.get(null));
    }

    @Test
    void getOrderNotFound() {
        Mockito.when(orderRepository.find(ID)).thenReturn(Optional.empty());

        Assertions.assertThrows(OrderNotFoundException.class, () -> orderService.get(ID));

        Mockito.verify(orderRepository).find(ID);
    }

    @Test
    void save() {
        Mockito.when(orderRepository.save(ORDER_WO_ID)).thenReturn(ORDER_WITH_ID);

        Truth.assertThat(orderService.save(ORDER_WO_ID)).isEqualTo(ORDER_WITH_ID);

        Mockito.verify(orderRepository).save(ORDER_WO_ID);
    }

    @Test
    void update() {
        Mockito.when(orderRepository.find(ID)).thenReturn(OPTIONAL_ORDER);
        Mockito.when(orderRepository.update(ORDER_WITH_ID)).thenReturn(ORDER_WITH_ID);

        Truth.assertThat(orderService.update(ORDER_WITH_ID)).isEqualTo(ORDER_WITH_ID);

        Mockito.verify(orderRepository).find(ID);
        Mockito.verify(orderRepository).update(ORDER_WITH_ID);
    }

    @Test
    void delete() {
        Mockito.when(orderRepository.find(ID)).thenReturn(OPTIONAL_ORDER);

        orderService.delete(ID);

        Mockito.verify(orderRepository).find(ID);
        Mockito.verify(orderRepository).delete(ID);
    }
}