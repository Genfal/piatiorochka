package ru.pivasic.carasic.service.impl;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.pivasic.carasic.database.api.exception.product.NullProductIdException;
import ru.pivasic.carasic.database.api.exception.product.ProductNotFoundException;
import ru.pivasic.carasic.database.api.model.Product;
import ru.pivasic.carasic.database.api.repository.ProductRepository;
import ru.pivasic.carasic.service.api.ProductService;

import java.util.List;
import java.util.Optional;

/**
 * @author TorstendasTost
 * @since 06.08.2022
 */
class ProductServiceImplTest {

    private static final Long ID = 1L;

    private static final Integer PAGE = 1;

    private static final Integer SIZE = 2;

    private static final Product PRODUCT_WITH_ID = Product.builder().id(ID).build();

    private static final Product PRODUCT_WO_ID = Product.builder().build();

    private static final List<Product> PRODUCT_LIST = List.of(Product.builder().build());

    private static final Optional<Product> OPTIONAL_PRODUCT = Optional.of(PRODUCT_WITH_ID);

    private ProductRepository productRepository;

    private ProductService productService;

    @BeforeEach
    void init() {
        productRepository = Mockito.mock(ProductRepository.class);
        productService = new ProductServiceImpl(productRepository);
    }

    @Test
    void getSuccess() {
        Mockito.when(productRepository.find(ID)).thenReturn(OPTIONAL_PRODUCT);

        Truth.assertThat(productService.get(ID)).isEqualTo(PRODUCT_WITH_ID);

        Mockito.verify(productRepository).find(ID);
    }

    @Test
    void getNullProductId() {
        Assertions.assertThrows(NullProductIdException.class, () -> productService.get(null));
    }

    @Test
    void getProductNotFound() {
        Mockito.when(productRepository.find(ID)).thenReturn(Optional.empty());

        Assertions.assertThrows(ProductNotFoundException.class, () -> productService.get(ID));

        Mockito.verify(productRepository).find(ID);
    }

    @Test
    void getAll() {
        Mockito.when(productRepository.findAll(PAGE, SIZE)).thenReturn(PRODUCT_LIST);

        Truth.assertThat(productService.getAll(PAGE, SIZE)).isEqualTo(PRODUCT_LIST);

        Mockito.verify(productRepository).findAll(PAGE, SIZE);
    }

    @Test
    void save() {
        Mockito.when(productRepository.save(PRODUCT_WO_ID)).thenReturn(PRODUCT_WITH_ID);

        Truth.assertThat(productService.save(PRODUCT_WO_ID)).isEqualTo(PRODUCT_WITH_ID);

        Mockito.verify(productRepository).save(PRODUCT_WO_ID);
    }

    @Test
    void update() {
        Mockito.when(productRepository.find(ID)).thenReturn(OPTIONAL_PRODUCT);
        Mockito.when(productRepository.update(PRODUCT_WITH_ID)).thenReturn(PRODUCT_WITH_ID);

        Truth.assertThat(productService.update(PRODUCT_WITH_ID)).isEqualTo(PRODUCT_WITH_ID);

        Mockito.verify(productRepository).find(ID);
        Mockito.verify(productRepository).update(PRODUCT_WITH_ID);
    }

    @Test
    void delete() {
        Mockito.when(productRepository.find(ID)).thenReturn(OPTIONAL_PRODUCT);

        productService.delete(ID);

        Mockito.verify(productRepository).find(ID);
        Mockito.verify(productRepository).delete(ID);
    }

    @Test
    void savePicture() {
    }

    @Test
    void deletePicture() {
    }
}