package ru.pivasic.carasic.service.impl;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.pivasic.carasic.database.api.exception.stack.NullStackIdException;
import ru.pivasic.carasic.database.api.exception.stack.StackNotFoundException;
import ru.pivasic.carasic.database.api.model.Stack;
import ru.pivasic.carasic.database.api.repository.StackRepository;
import ru.pivasic.carasic.service.api.StackService;

import java.util.Optional;

/**
 * @author TorstendasTost
 * @since 04.08.2022
 */
class StackServiceImplTest {

    private static final Long ID = 1L;

    private static final Stack STACK_WITH_ID = Stack.builder().id(ID).build();

    private static final Stack STACK_WO_ID = Stack.builder().build();

    private static final Optional<Stack> OPTIONAL_STACK = Optional.of(STACK_WITH_ID);

    private StackRepository stackRepository;

    private StackService stackService;

    @BeforeEach
    void init() {
        stackRepository = Mockito.mock(StackRepository.class);
        stackService = new StackServiceImpl(stackRepository);
    }

    @Test
    void getSuccess() {
        Mockito.when(stackRepository.find(ID)).thenReturn(OPTIONAL_STACK);

        Truth.assertThat(stackService.get(ID)).isEqualTo(STACK_WITH_ID);

        Mockito.verify(stackRepository).find(ID);
    }

    @Test
    void getNullOrderId() {
        Assertions.assertThrows(NullStackIdException.class, () -> stackService.get(null));
    }

    @Test
    void getOrderNotFound() {
        Mockito.when(stackRepository.find(ID)).thenReturn(Optional.empty());

        Assertions.assertThrows(StackNotFoundException.class, () -> stackService.get(ID));

        Mockito.verify(stackRepository).find(ID);
    }

    @Test
    void save() {
        Mockito.when(stackRepository.save(STACK_WO_ID)).thenReturn(STACK_WITH_ID);

        Truth.assertThat(stackService.save(STACK_WO_ID)).isEqualTo(STACK_WITH_ID);

        Mockito.verify(stackRepository).save(STACK_WO_ID);
    }

    @Test
    void update() {
        Mockito.when(stackRepository.find(ID)).thenReturn(OPTIONAL_STACK);
        Mockito.when(stackRepository.update(STACK_WITH_ID)).thenReturn(STACK_WITH_ID);

        Truth.assertThat(stackService.update(STACK_WITH_ID)).isEqualTo(STACK_WITH_ID);

        Mockito.verify(stackRepository).find(ID);
        Mockito.verify(stackRepository).update(STACK_WITH_ID);
    }

    @Test
    void delete() {
        Mockito.when(stackRepository.find(ID)).thenReturn(OPTIONAL_STACK);

        stackService.delete(ID);

        Mockito.verify(stackRepository).find(ID);
        Mockito.verify(stackRepository).delete(ID);
    }
}