package ru.pivasic.carasic.shop.app.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pivasic.carasic.commons.api.dto.UserDtoAuthentication;

/**
 * Контроллер для аутентификации и регистрации клиентов
 *
 * @author Viktor Konev
 * @since 31.07.2022
 */
@RestController
@RequestMapping("/v1/shop/authentication")
public class AuthenticationController {

    @PostMapping("/registration")
    public String registration(@RequestBody UserDtoAuthentication userDtoAuthentication) {
        throw new RuntimeException("Not implemented");
    }

    @PostMapping("/login")
    public String login(@RequestBody UserDtoAuthentication userDtoAuthentication) {
        throw new RuntimeException("Not implemented");
    }
}
