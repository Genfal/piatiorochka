package ru.pivasic.carasic.shop.app.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Контроллер для работы с аватаром
 *
 * @author Viktor Konev
 * @since 31.07.2022
 */
@RestController
@RequestMapping("/v1/shop/avatar")
public class AvatarController {

    @PostMapping("/{id}")
    public void setAvatar(@PathVariable Long id) {
        throw new RuntimeException("Not implemented");
    }

    @GetMapping("/{id}")
    public String getAvatar(@PathVariable Long id) {
        throw new RuntimeException("Not implemented");
    }

    @DeleteMapping("/{id}")
    public void deleteAvatar(@PathVariable Long id) {
        throw new RuntimeException("Not implemented");
    }
}
