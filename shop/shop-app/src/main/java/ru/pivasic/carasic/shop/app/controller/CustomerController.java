package ru.pivasic.carasic.shop.app.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pivasic.carasic.database.api.model.Customer;

import java.util.List;

/**
 * Контроллер для работы с покупателем
 *
 * @author Viktor Konev
 * @since 31.07.2022
 */
@RestController
@RequestMapping("/v1/shop/customers")
public class CustomerController {

    @PostMapping
    public Customer createCustomer(@Validated @RequestBody Customer customer) {
        throw new RuntimeException("Not implements");
    }

    @GetMapping("/{id}")
    public List<Customer> getCustomer(@PathVariable Long id) {
        throw new RuntimeException("Not implements");
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable Long id) {
        throw new RuntimeException("Not implements");
    }

    @PutMapping
    public void updateCustomer(@Validated @RequestBody Customer customer) {
        throw new RuntimeException("Not implements");
    }
}
