package ru.pivasic.carasic.shop.app.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pivasic.carasic.commons.api.dto.CustomerAndOrderDto;
import ru.pivasic.carasic.database.api.model.Order;

import java.util.List;

/**
 * Контроллер для работы с заказом
 *
 * @author Viktor Konev
 * @since 31.07.2022
 */
@RestController
@RequestMapping("/v1/shop/orders")
public class OrderController {

    @PostMapping
    public void createOrder(@Validated @RequestBody CustomerAndOrderDto customerAndOrderDto) {
        throw new RuntimeException("Not implemented");
    }

    @GetMapping("/{id}")
    public List<Order> getOrderList(@PathVariable Long id) {
        throw new RuntimeException("Not implemented");
    }
}
