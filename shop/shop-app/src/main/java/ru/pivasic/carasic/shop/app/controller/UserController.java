package ru.pivasic.carasic.shop.app.controller;

import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Контроллер для работы с клиентом
 *
 * @author Viktor Konev
 * @since 31.07.2022
 */
@RestController
@RequestMapping("/v1/shop/users")
public class UserController {

    @PatchMapping("/{id}/password")
    public void updatePassword(@PathVariable Long id, @RequestBody String password) {
        throw new RuntimeException("Not implements");
    }
}
